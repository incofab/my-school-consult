<?php 

$page = isset($page) ? $page : '';
$home = ''; $services = ''; $scholarship = ''; $news = ''; $internship = ''; $contact = ''; $login = ''; $register = '';

$active = 'active';

if($page == 'home') $home = $active; 
if($page == 'services') $services = $active; 
if($page == CATEGORY_SCHOLARSHIP) $scholarship = $active; 
if($page == CATEGORY_GENERAL) $news = $active; 
if($page == CATEGORY_INTERNSHIP) $internship = $active; 
if($page == 'contact') $contact = $active; 
if($page == 'login') $login = $active; 
if($page == 'register') $register = $active; 

?>

<!DOCTYPE html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>blog</title>
    
    <!-- Bootstrap Core CSS -->
    {{Html::style('css/bootstrap.min.css')}}

    <!-- Custom CSS -->
    {{Html::style('css/footer.css')}}
    {{Html::style('css/shadow.css')}}
    {{Html::style('css/style.css')}}
    {{Html::style('css/style1.css')}}

    <!-- Custom Fonts -->
    {{Html::style('font-awesome/css/font-awesome.min.css')}}




    <!-- jQuery -->
    {{Html::script('js/jquery.js') }}
    
     <script type="text/javascript">
		    $(document).ready(function () {
		$('.navbar .dropdown').hover(function () {
		        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
		    }, function () {
		        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(105)
		    });
		});

	</script>
	<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : 'your-app-id',
      xfbml      : true,
      version    : 'v2.9'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<body>


 <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <img src="{{URL::asset('img/myschoolconsult.png')}}" class="nav-brand">
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav" style="margin-left: 10px;">
            
            <li class="{{$home}}"><a href="{{ route('home') }}">Home</a></li>
            
            <li class="dropdown {{$services}}">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('view_store_section', CATEGORY_STORE_COMPUTER_SOFTWARE) }}">CBT Practice Software</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{ route('view_store_section', CATEGORY_STORE_MOBILE_APP) }}">CBT Mobile Android App</a></li>   
                <li role="separator" class="divider"></li>              
                <li><a href="">CBT Software Agent/Reseller</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">CBT Mobile App Agent/Reseller</a></li>
                <li role="separator" class="divider"></li>
              </ul>
            </li>
            
             <li class="dropdown {{$scholarship}}">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Scholarship<span class="caret"></span></a>
              <ul class="dropdown-menu">
                 <li><a href="{{ route('view_sub_category', [CATEGORY_SCHOLARSHIP, SUB_CAT_UNDERGRADUATE_SCHOLARSHIP]) }}">Undergraduate Scholarship</a></li>
                 <li role="separator" class="divider"></li>
                 <li><a href="{{ route('view_sub_category', [CATEGORY_SCHOLARSHIP, SUB_CAT_GRADUATE_SCHOLARSHIP]) }}">Graduate Scholarship</a></li>
                 <li role="separator" class="divider"></li>
                 <li><a href="{{ route('view_sub_category', [CATEGORY_SCHOLARSHIP, SUB_CAT_INTERNATIONAL_SCHOLARSHIP]) }}">International Scholarship</a></li>
                 <li role="separator" class="divider"></li>
                 <li><a href="{{ route('view_sub_category', [CATEGORY_SCHOLARSHIP, SUB_CAT_POST_SCHOLARSHIP]) }}">Postgraduate Scholarship</a></li>
                <li role="separator" class="divider"></li>
              </ul>
            </li>
           
            
             <li class="dropdown {{$news}}">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">News<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('view_sub_category', [CATEGORY_GENERAL, SUB_CAT_LATEST_NEWS]) }}">Latest News</a></li>
                 <li role="separator" class="divider"></li>
                <li><a href="{{ route('view_sub_category', [CATEGORY_GENERAL, SUB_CAT_UNIVERSITY_IREPORT]) }}">University I-report</a></li>
                 <li role="separator" class="divider"></li>
                <li><a href="{{ route('view_sub_category', [CATEGORY_GENERAL, SUB_CAT_JAMB_NEWS]) }}">Latest Jamb news</a></li>
                 <li role="separator" class="divider"></li>
                 <li><a href="{{ route('view_sub_category', [CATEGORY_GENERAL, SUB_CAT_NYSC_NEWS]) }}">Nysc News</a></li>
                 <li role="separator" class="divider"></li>
                 <li><a href="{{ route('view_sub_category', [CATEGORY_GENERAL, SUB_CAT_GIST]) }}">Gist</a></li>
                  <li><a href="forum">Gist</a></li>
              </ul>
            </li>
            
            <li class="dropdown {{$internship}}">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Internship<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ route('view_sub_category', [CATEGORY_INTERNSHIP, SUB_CAT_INDUSTRIAL_TRAINING]) }}">Industrial Training</a></li>
                 <li role="separator" class="divider"></li>
                <li><a href="{{ route('view_sub_category', [CATEGORY_INTERNSHIP, SUB_CAT_UNDERGRADUATE_INTERNSHIP]) }}">Undergraduate Internship</a></li>
                 <li role="separator" class="divider"></li>
                <li><a href="{{ route('view_sub_category', [CATEGORY_INTERNSHIP, SUB_CAT_GRADUATE_INTERNSHIP]) }}">Graduate Intenship</a></li>
                 <li role="separator" class="divider"></li>
                 <li><a href="#"></a></li>
              </ul>
            </li>
          </ul>
          
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown {{$contact}}">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contact<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="contact">Contact Information</a></li>
                 <li role="separator" class="divider"></li>
                <li><a href="">Advertise at MyschoolConsult</a></li>

                
              </ul>
            </li>
            <li><p class="navbar-text" style="color:white;">Already have an account?</p></li>
        <li class="dropdown {{$login}}">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
      <ul id="login-dp" class="dropdown-menu" style="padding: 3px 10px;">
        <li>
           <div class="row">
              <div class="col-md-12">
                Login via
                <div class="social-buttons">
                  <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                  <a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a>
                </div>
                                or
                 <form class="form" role="form" method="post" action="{{ url('/login') }}" accept-charset="UTF-8" id="login-nav">
                 	{{ csrf_field() }}
                    <div class="form-group">
                       <label class="sr-only" for="exampleInputEmail2">Email address</label>
                       <input name="email" type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                    </div>
                    <div class="form-group">
                       <label class="sr-only" for="exampleInputPassword2">Password</label>
                       <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required
                       		name="password">
                                             <div class="help-block text-right"><a href="">Forget the password ?</a></div>
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                    </div>
                    <div class="checkbox">
                       <label>
                       <input type="checkbox"> keep me logged-in
                       </label>
                    </div>
                 </form>
              </div>
              <div class="bottom text-center">
                New here ? <a href="{{url('/register')}}"><b>Join Us</b></a>
              </div>
           </div>
        </li>
      </ul>
        </li>

          </ul>
          
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <br>
    <br>
      <br /><br /><br />
    <div class="row">
   
    
      
   
      

    </div>
   
<div class="container body">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td width="68%" align="left">You are here: <a href="https://myschoolconsult.com.ng/">Myschoolconsult</a></td>
<td width="32%" align="right">| <a href="http:consultblog/public/contact">Report this page to Admin</a> | <strong><?php 
         date_default_timezone_set('Africa/Lagos');
        $today = date(" F j(D), Y");
        echo $today;
        ?></strong></td>
</tr>
</tbody></table>
</div>


        <br /><br />

        