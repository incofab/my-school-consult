<?php 
$page = CATEGORY_GENERAL;
$num = 50;  
?>

@extends('blog.layout')


@section('home_content')

    <!-- Navigation -->
    
    <!-- Page Content -->
    <div class="container body">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-md-8 ">
              <div class="shadowstore2">
           
                 <h2 class="head text-center">Latest School News</h2>
                           
                           <div class="container ">
                             <div class="row storebody">
                              <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                 <tr>
                                    <td class="table1">
                                      <a href=""><span class="heading8">Secret to High JAMB Mock Results 2017 - Share Yours</span></a><br>
                                      <span>by <strong><a href="">MyschoolConsult</a></strong> [04-May-2017 17:36:15]<span class="infoText1"> for <a href="">school</a></span></span>
                                      | Comments [133]
                                    </td>
                                  </tr>
                                 
                                  </tbody>
                                  </table>
                                  <hr>


                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="table1">
                                <tbody>
                                 <tr>
                                    <td class="table1">
                                      <a href=""><span class="heading8">Secret to High JAMB Mock Results 2017 - Share Yours</span></a><br>
                                      <span>by <strong><a href="">MyschoolConsult</a></strong> [04-May-2017 17:36:15]<span class="infoText1"> for <a href="">school</a></span></span>
                                      | Comments [133]
                                    </td>
                                  </tr>
                                  </tbody>
                                  </table>
                                           
                                  </div>
                             
                            
                        </div>
  
                               
                                
                        

               

                 <div class="text_reading_format" style="clear:left;">
		<br>
		 
		To Practice JAMB 2017 CBT online for free - <a href="">Click Here</a>
		<br>
		<br>
		To Download CBT Mobile App - <a href="">Click Here</a><br>
		<br>
		To Download CBT Software for Computer Systems - <a href="" target="_blank">Click Here</a>
		</div>
		<hr>
		<hr>
		<hr>
	</div>

	<div class="row">
           <div class="col-md-6">
           <div class=" portfolio-item indexshadow2">
             <div class="head text-center">Latest News  </div>
             <ul class="list-group">
 			  @foreach($latestNews as $news)
					<li class="list-group-item">
						<a href="{{ route('view_post', $news[TITLE_URL]) }}"
							>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a>
					</li>
              @endforeach
              </ul>
                         
              
                   
              
              
          
              
              <div class="head text-center">Admission News</div>
              <ul class="list-group">
 			  @foreach($admissionNews as $news)
					<li class="list-group-item">
						<a href="{{ route('view_post', $news[TITLE_URL]) }}"
							>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a>
					</li>
              @endforeach
              </ul>
            </div>
            </div>


            <div class="col-md-6">
            <div class=" portfolio-item indexshadow2">
                 <div class="head text-center">Jamb News</div>
             <ul class="list-group ">
               @foreach($jambNews as $news)
					<li class="list-group-item">
						<a href="{{ route('view_post', $news[TITLE_URL]) }}"
							>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a>
					</li>
              @endforeach
              </ul>


              <div class="head text-center">Post UTME Screening</div>
              
              <ul class="list-group">
 			  @foreach($postUTMENews as $news)
					<li class="list-group-item">
						<a href="{{ route('view_post', $news[TITLE_URL]) }}"
							>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a>
					</li>
              @endforeach
              </ul>
			
			</div>
            </div>
        	</div> <!--  // .row -->
        	
            </div>
            

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">
            	<div class="shadowsideba2">
	                <!-- Blog Search Well -->
					@include('blog.' . $folder . '.sidebar')
            	</div>
            </div>

        </div>
        <!-- /.row -->


    </div>
    <!-- /.container -->


@stop