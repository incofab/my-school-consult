<br><br>
<!--footer-->
<div class="foot">
<footer class="footer1">
<div class="container">

<div class="row"><!-- row -->
            
                <div class="col-lg-3 col-md-3"><!-- widgets column left -->
                <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                            <li class="widget-container widget_nav_menu"><!-- widgets list -->
                    
                                <h1 class="title-widget">MyschoolConsult Services</h1>
                                
                                <ul>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> CBT Practice Software</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> CBT Mobile Android App</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> CBT Software Agent/Reseller</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> CBT Mobile App Agent/Reseller</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> JAMB Result Slip - Get Yours</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">New Download Project Topics Here </span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> Myschoolconsult Bank Accounts  </span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> Advertise on Myschoolconsult</span></a></li>
                                </ul>
                                <br>


                                <h1 class="title-widget">MyschoolConsult Services</h1>
                                
                                <ul>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> CBT Practice Software</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> CBT Mobile Android App</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> CBT Software Agent/Reseller</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> CBT Mobile App Agent/Reseller</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> JAMB Result Slip - Get Yours</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">New Download Project Topics Here </span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> Myschoolconsult Bank Accounts  </span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> Advertise on Myschoolconsult</span></a></li>
                                </ul>
                    
                    
                            </li>
                            
                        </ul>

                      
                </div><!-- widgets column left end -->
                
                
                
                <div class="col-lg-3 col-md-3"><!-- widgets column left -->
            
                <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                            <li class="widget-container widget_nav_menu"><!-- widgets list -->
                            
                    			<style>.black{ color: black;}</style>
                                <h1 class="title-widget"><a class="black" href="<?= route('view_category', [CATEGORY_SCHOLARSHIP]) ?>">Scholarship</a></h1>
                                
                                <ul class="b">
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">All Scholarships</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> International Scholarships</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">Local Scholarships</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> Undergraduate Scholarships</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> Postgraduate Scholarships</span></a></li>
                                    
                                    
                                </ul>
                                       <br>

                                <h1 class="title-widget"><a class="black" href="<?= route('view_category', [CATEGORY_INTERNSHIP]) ?>">Intenship</a></h1>
                                
                                <ul class="b">
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">Graduate</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">Under graduate</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">Industrial Training</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> Undergraduate Scholarships</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> Postgraduate Scholarships</span></a></li>
                                   
                                    
                                </ul>
                                <br>
                                <h1 class="title-widget"><a class="black" href="#">Past Question</a></h1>
                                
                                <ul class="b">
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">Cbt Practice Software</span></a></li>
                                    
                                   
                                    
                                </ul>
                    

                            </li>
                            
                        </ul>
          
                </div><!-- widgets column left end -->
                
                
                
                <div class="col-lg-3 col-md-3"><!-- widgets column left -->
            
                <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                            <li class="widget-container widget_nav_menu"><!-- widgets list -->


                                <h1 class="title-widget"><a class="black" href="<?= route('view_category', [CATEGORY_GENERAL]) ?>">News</a></h1>
                                
                           
                            <ul class="b">
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">Latest News</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">I Report News</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">Latest on Admission Lists</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">Latest JAMB News</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">Latest Gist & Gossip    </span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i>6 </a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i> 7</a></li>
                                    <li><a  href="#" target="_blank"><i class="fa fa-angle-double-right b"></i></a></li>
                                    
                         </ul>

                            <h1 class="title-widget"><a class="black" href="<?= route('view_our_products') ?>">Store</a></h1>
                                
                           
                            <ul class="b">
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b"> Enter Myschool Store</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">Books (9)</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">Project & Research Works (4941)</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">Scratch Cards (1)</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i><span class="b">JAMB (4)</span></a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i>6 </a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right b"></i> 7</a></li>
                                    <li><a  href="#" target="_blank"><i class="fa fa-angle-double-right b"></i></a></li>
                                    
                         </ul>
                            </li>
                            
                        </ul>
              
                </div><!-- widgets column left end -->
                
                
                <div class="col-lg-3 col-md-3"><!-- widgets column center -->
                
                   
                    
                        <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                            <li class="widget-container widget_recent_news"><!-- widgets list -->
                    
                                <h1 class="title-widget">For Updates </h1>
                                
                                <div class="footerp"> 
                                
                                <h2 class="title-median"> <img src="{{URL::asset('img/myschoolconsult.png')}}"></h2>
                                <p><b>Email id:</b> <a href="">info</a></p>
                                <p><b>Helpline Numbers </b>

    <b style="color:#ffc106;">(AM to PM):</b>  </p>
    
    <p><b> Office / Postal Address</b></p>
    <p><b>Phone Numbers : </b></p>
    <p> </p>
                                </div>
                                
                                <div class="social-icons">
                                
                                    <ul class="nomargin">
                                    
                <a href="https://www.facebook.com/"><i class="fa fa-facebook-square fa-3x social-fb" id="social"></i></a>
                <a href="https://twitter.com/"><i class="fa fa-twitter-square fa-3x social-tw" id="social"></i></a>
                <a href="https://plus.google.com/"><i class="fa fa-google-plus-square fa-3x social-gp" id="social"></i></a>
                <a href=""><i class="fa fa-envelope-square fa-3x social-em" id="social"></i></a>
                                    
                                    </ul>
                                </div>
                            </li>
                          </ul>
                       </div>

                </div>
</div>
</footer>
<!--header-->

<div class="footer-bottom">

    <div class="container">

        <div class="row">

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                <div class="copyright">

                    © 2017 MyschoolConsult, All rights reserved

                </div>

            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                <div class="design">

                     <a href="#"></a> |  <a target="_blank" href="http://"></a>

                </div>

            </div>

        </div>

    </div>

</div>
</div>


    <!-- Bootstrap Core JavaScript -->
    {{Html::script('js/bootstrap.min.js') }}
    
    
</body>

</html>