@extends('blog.layout')


	@section('home_content')

    <!-- Navigation -->
    
    <!-- Page Content -->
    <div class="container top">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8 shadowpost1">
           
                 <div class="head "><h2>My Profile</h2></div>
                           
                <div class="container">

<h1 class="no_spacer"><?= $profile[NAME]?></h1>
<br/>



                        <h1>User Profile</h1>
                        <hr>
                      <div class="row">
                          <!-- left column -->
                          <div class="col-md-3">
                            <div class="text-center profilephoto1">
                              <img src="//placehold.it/100" class="avatar img-circle img-responsive" alt="avatar">
                              
                            </div>
                          </div>
                          
                          <!-- edit form column -->
                          <div class="col-md-9 personal-info">
                            <div class="alert alert-info alert-dismissable">
                              <a class="panel-close close" data-dismiss="alert">×</a> 
                              <i class="fa fa-coffee"></i>
                              This is an <strong>.alert</strong>. Use this to show important messages to the user.
                            </div>
                            <h3>Personal information</h3>
                            
                            <form class="form-horizontal" role="form">
                              <div class="form-group">
                                <label class="col-lg-3">Full name:</label>
                                <div class="col-lg-8">
                                  <span><?= $profile[NAME]?></span>
                                </div>
                              </div>
                                <div class="form-group">
                                <label class="col-lg-3">Gender:</label>
                                <div class="col-lg-8">
                                  <div class="ui-select">
                                   Male
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-lg-3">Mobile:</label>
                                <div class="col-lg-8">
                                  <div class="ui-select">
                                   080909988999
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-lg-3">Email:</label>
                                <div class="col-lg-8">
                                  <div class="ui-select">
                                   <?= $profile[NAME] ?>
                                  </div>
                                </div>
                              </div>
                            
                              <br>
                                <h3>School infomation</h3>
                                <br>
                              <div class="form-group">
                                <label class="col-lg-3">School Type:</label>
                                <div class="col-lg-8">
                                  <div class="ui-select">
                                   Graduate
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-lg-3">School:</label>
                                <div class="col-lg-8">
                                  <div class="ui-select">
                                   Uniport
                                  </div>
                                </div>
                              </div>
                               <div class="form-group">
                                <label class="col-lg-3">Level:</label>
                                <div class="col-lg-8">
                                  <div class="ui-select">
                                   Graduate
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-lg-3">Department:</label>
                                <div class="col-lg-8">
                                  <div class="ui-select">
                                   Computer Science
                                  </div>
                                </div>
                              </div>
                              <br> 
                              <h3>Account information</h3>
                              <br>

                             
                             
                            </form>
                          </div>
                      </div>
                    </div>
                    <hr>
                               
                                
                        

               

                 <div class="text_reading_format" style="clear:left;">
<br>
 
To Practice JAMB 2017 CBT online for free - <a href="">Click Here</a>
<br>
<br>
To Download CBT Mobile App - <a href="">Click Here</a><br>
<br>
To Download CBT Software for Computer Systems - <a href="" target="_blank">Click Here</a>
</div>

<hr /><hr /><hr />

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4 shadowpost2">
                <!-- Blog Search Well -->
                @include('blog.storesidebar')
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->


@stop
