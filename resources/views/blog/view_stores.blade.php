<?php $page = 'services'; ?>

@extends('blog.layout')

@section('home_content')

<!-- Page Content -->
<div class="container body top">

	<div class="row">
        <!-- Blog Post Content Column -->
		<div class="col-lg-8 shadowstore">
           
			<div class="head ">School Store</div>
			 <div class="container ">
			 <?php 
				$len = count($products);
				for ($i = 0; $i < $len; $i++): ?>
			
				<div class="row">
					<div class="col-lg-4">
						<img src="{{URL::asset('/photos/shares/store/' . $products[$i][IMAGES]) }}" alt="cbt"><br>
						<a href="{{route('view_product', $products[$i][TITLE])}}" class="text-center"> {{ $products[$i][TITLE] }}</a><br>
						<span class="text-center price">₦{{$products[$i][PRICE]}}</span>
					</div>
					<?php $i++; if($i >= $len) break; ?>
					<div class="col-lg-4">
						<img src="{{URL::asset('/photos/shares/store/' . $products[$i][IMAGES]) }}" alt="cbt"><br>
						<a href="{{route('view_product', $products[$i][TITLE])}}" class="text-center"> {{ $products[$i][TITLE] }}</a><br>
						<span class="text-center price">₦{{$products[$i][PRICE]}}</span>
					</div>
					
				</div>

			<?php	endfor; ?>
				</div>

		


			<div class="text_reading_format" style="clear:left;">
				<br>
				To Practice JAMB 2017 CBT online for free - <a href="">Click Here</a>
				<br>
				<br>
				To Download CBT Mobile App - <a href="">Click Here</a><br>
				<br>
				To Download CBT Software for Computer Systems - <a href="" target="_blank">Click Here</a>
			</div>
			<hr>

		</div>
			

		<!-- Blog Sidebar Widgets Column -->
		<div class="col-lg-4 shadowsideba">
			@include('sidebar.stores_sidebar')
		</div>
		</div>
		
		
 <!-- /.row -->
	<hr>

</div>
<!-- /.container -->

@endsection