<?php
	$page = CATEGORY_SCHOLARSHIP;
	$num = 50;
?>

@extends('blog.layout')


@section('home_content')


    <!-- Navigation -->
    
    <!-- Page Content -->
    <div class="container body">
        <div class="row">
            <!-- Blog Post Content Column -->
            <div class="col-md-8">
              <div class=" shadowstore">
                 <h3 class="head text-center">Latest Scholarship News</h3>
                 <br>
                 <br>
                 <div class="row">
                 <div class="col-md-6">
                 <a href=""><h4><i class="fa fa-caret-right" aria-hidden="true">Undergraduate Scholarship</i></h4></a>
                 </div>
                 <div class="col-md-6">
                 <a href=""><h4><i class="fa fa-caret-right" aria-hidden="true">Post Scholarship</i></h4></a>
                 </div>
                 </div>
                  <div class="row">
                 <div class="col-md-6">
                 <a href=""><h4><i class="fa fa-caret-right" aria-hidden="true">International Scholarship</i></h4></a>
                 </div>
                 <div class="col-md-6">
                 <a href=""> <h4>Scholarship</h4></a>
                 </div>
                 </div>
                           
                           <div class="container ">
                             <div class="row storebody">
                              <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                 <tr>
                                    <td class="table1">
                                      <a href=""><span class="heading8">Secret to High JAMB Mock Results 2017 - Share Yours</span></a><br>
                                      <span>by <strong><a href="">MyschoolConsult</a></strong> [04-May-2017 17:36:15]<span class="infoText1"> for <a href="">school</a></span></span>
                                      | Comments [133]
                                    </td>
                                  </tr>
                                 
                                  </tbody>
                                  </table>
                                  <hr>


                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="table1">
                                <tbody>
                                 <tr>
                                    <td class="table1">
                                      <a href=""><span class="heading8">Secret to High JAMB Mock Results 2017 - Share Yours</span></a><br>
                                      <span>by <strong><a href="">MyschoolConsult</a></strong> [04-May-2017 17:36:15]<span class="infoText1"> for <a href="">school</a></span></span>
                                      | Comments [133]
                                    </td>
                                  </tr>
                                  </tbody>
                                  </table>
                                           
                                  </div>
                             
                            
                        </div>

		<div class="text_reading_format" style="clear:left;">
		<br>
		To Practice JAMB 2017 CBT online for free - <a href="">Click Here</a>
		<br>
		<br>
		To Download CBT Mobile App - <a href="">Click Here</a><br>
		<br>
		To Download CBT Software for Computer Systems - <a href="" target="_blank">Click Here</a>
		</div>
		<hr>
		<hr>
		<hr>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="portfolio-item indexshadow2">
			<div class="head text-center">Undergraduate Scholarship</div>
				<ul class="list-group">
				  @foreach($underGraduateScholarship as $news)
					<li class="list-group-item">
						<a href="{{ route('view_post', $news[TITLE_URL]) }}"
							>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a>
						</li>
		              @endforeach
	            </ul>
	              
	            <div class="head text-center">Graduate Scholarship</div>
	              <ul class="list-group">
	 				  @foreach($graduateScholarship as $news)
						<li class="list-group-item">
							<a href="{{ route('view_post', $news[TITLE_URL]) }}"
								>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a>
						</li>
		              @endforeach
	              </ul>
			</div>
		</div>
		<div class="col-md-6">
			<div class="portfolio-item indexshadow2">
	                 <div class="head text-center">International Scholarship</div>
	             <ul class="list-group ">
		 			  @foreach($internationalScholarship as $news)
							<li class="list-group-item">
								<a href="{{ route('view_post', $news[TITLE_URL]) }}"
									>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a>
							</li>
		              @endforeach
	              </ul>
	
	              <div class="head text-center">Post Graduate Scholarship</div>
	              <ul class="list-group">
	 			  @foreach($postGraduateScholarship as $news)
						<li class="list-group-item">
							<a href="{{ route('view_post', $news[TITLE_URL]) }}"
								>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a>
						</li>
	              @endforeach
	              </ul>
			</div>
		</div>
		</div>
	</div>
            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">
            	<div class="shadowsideba2">
            	<!-- Blog Search Well -->
				@include('blog.' . $folder . '.sidebar')
            	</div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
@stop