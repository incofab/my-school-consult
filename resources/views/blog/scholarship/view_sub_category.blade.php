<?php
	$page = CATEGORY_SCHOLARSHIP;
	$num = 70;  
?>

@extends('blog.layout')


@section('home_content')


	<!-- Page Content -->
	<div class="container body">
	   
		<div class="row">
			<div class="col-md-8">
			<div class=" portfolio-item shadowstore">
				<div class="head text-center">{{ $heading }}</div>
				<ul class="list-group">
					@foreach($posts as $news)
					<li class="list-group-item">
						<h4><a href="{{ route('view_post', $news[TITLE_URL]) }}"
							>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a></h4>
					</li>
					@endforeach
				</ul>
			</div>
			</div>
		<div class="col-lg-4">
			 <div class="shadowsideba">
			@include('sidebar.view_scholar_sidebar')
			 </div>
		</div>
			
		</div>		
	    
	</div>

@stop