<?php $page = 'home'; ?>
@extends('blog.layout')


	@section('home_content')

 
        <!-- Projects Row -->
        <div class="container body">
        <div class="row">

			<?php $num = 50;  ?>
            <div class="col-md-4">
            <div class="portfolio-item indexshadow">
             <div class="head text-center">Latest News  </div>
             <ul class="list-group">
				
					@foreach($latestNews as $news)
					
						<li class="list-group-item"><a href="{{ route('view_post', $news[TITLE_URL]) }}"
							>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a></li>
							
					@endforeach

			</ul>

			<div class="head text-center">Student Intenship Opportunities</div>
				<ul class="list-group">
 
					@foreach($internshipNews as $news)
					
						<li class="list-group-item"><a href="{{ route('view_post', $news[TITLE_URL]) }}"
							>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a></li>
							
					@endforeach
						
				</ul>
			</div>	
            </div>


            <div class="col-md-4">
			<div class="portfolio-item indexshadow">
                 <div class="head text-center">Myschconsult Gist</div>
					
					  <div style="font-size:20px; color:white; margin-bottom: 10px; text-align: center;" class="btn orange">Post a gist Free!</div><br>
			              <ul class="list-group">
 
							<li class="list-group-item"><a href="">Apply School News for University(2)</a></li>
							<li class="list-group-item"> <a href="">2017 JAMB UTME Mock Exam Holds in 3 weeks(4)</a></li>
							<li class="list-group-item"><a href="">Apply School News for University(4)</a></li>
							<li class="list-group-item"><a href="">2017 JAMB UTME Mock Exam Holds in 3 weeks(3)</a></li>
							<li class="list-group-item"><a href="">Apply School News for University(8)</a></li>
							<li class="list-group-item"><a href="">Apply School News for University(2)</a></li>
							<li class="list-group-item"><a href="">2017 JAMB UTME Mock Exam Holds in 3 weeks(4)</a></li>
							<li class="list-group-item"><a href="">Apply School News for University(4)</a></li>
						  </ul>

						  <a href="#">
                    <img class="img-responsive imgads" style="margin-left: 40px;" src="img/jmbads.jpg" alt="">
                </a>

			</div>
            </div>


            <div class="col-md-4">
			<div class="portfolio-item indexshadow">
                <div class="head text-center">University <img src="img/ireport_logo.png" height="60px"></div>
			    <ul class="list-group ">
			        @foreach($iReportNews as $news)
					
						<li class="list-group-item"><a href="{{ route('view_ireport', $news[TITLE_URL]) }}"
							>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a></li>
							
					@endforeach
  				</ul>

               
				  <div class="topic">
				  

				  </div>
				  
				  
			  <div class="head text-center">Nysc Forum</div>
				<ul class="list-group">
					@foreach($nyscNews as $news)
					
						<li class="list-group-item"><a href="{{ route('view_post', $news[TITLE_URL]) }}"
							>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a></li>
							
					@endforeach
			  	</ul>
			 </div>
             </div>
        
        <!-- /.row -->
          
			<div class="indexshadowstore">
				<div class="head ">School Store</div>
				<div class="row">
				<?php $num_ = 30; ?>
				@foreach($products as $product)
					<div class="col-md-2 software">
						<img src="{{ URL::asset('/photos/shares/store/' . $product[IMAGES]) }}" alt="cbt" >
						<br />
						<a href="{{route('view_product', $product[TITLE])}}" class="text-center" 
							>{{((strlen($product[TITLE]) > $num_) ? (substr($product[TITLE], 0, $num_) . '...') : $product[TITLE])}}</a><br>
						<span class="text-center price">₦{{$product[PRICE]}}</span>
					</div>
				@endforeach
				</div>
         		<br><br>
		  	 </div>
			 

			 <div class="col-md-4">
			 <div class="portfolio-item indexshadow1">
			 <div class="head forum text-center">Scholarship News</div>
                 <ul class="list-group ">
					@foreach($scholarshipNews as $news)
						<li class="list-group-item"><a href="{{ route('view_post', $news[TITLE_URL]) }}"
							>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a></li>
					@endforeach
					</ul>
                  <a href="#">
                    <img class="img-responsive imgads" style="margin-right: 40px;marging-top:60px;" src="img/testads.jpg" alt="">
                </a>
			</div>
            </div>
            
            <div class="col-md-4">
			<div class="portfolio-item indexshadow1">
                 <div class="head forum text-center">School Question and Answer</div>
                           <ul class="list-group">
							<li class="list-group-item">[Uniport] <a href="">Apply School News for University(2)</a></li>
							<li class="list-group-item"> <a href="">2017 JAMB UTME Mock Exam Holds in 3 weeks(4)</a></li>
							<li class="list-group-item">[IMSU] <a href="">Apply School News for University(4)</a></li>
							<li class="list-group-item"> <a href="">2017 JAMB UTME Mock Exam Holds in 3 weeks(3)</a></li>
							<li class="list-group-item">[UNN] <a href="">Apply School News for University(8)</a></li>
							<li class="list-group-item"> <a href="">2017 JAMB UTME Mock Exam Holds in 3 weeks(5)</a></li>
							<li class="list-group-item">[ABSU] <a href="">Apply School News for University(4)</a></li>
							<li class="list-group-item"> <a href="">2017 JAMB UTME Mock Exam Holds in 3 weeks(7)</a></li>
						  </ul>
				<a href="#">
                    <img class="img-responsive" src="http://placehold.it/700x400" alt="">
                </a>
			</div>
            </div>

             <div class="col-md-4">
	             <div class="portfolio-item indexshadow1">
	             <br>
	             <br>
	             <br>
                <div class="fb-page" data-href="https://www.facebook.com/uchennaLive/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/uchennaLive/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/uchennaLive/">Iroegbu Uchenna - Divine</a></blockquote></div>
            </div>
		</div>
	</div>
</div>

						  	 


@stop