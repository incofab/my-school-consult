<?php
$page = CATEGORY_INTERNSHIP;
$num = 80;
?>

@extends('blog.layout')


@section('home_content')


	<!-- Page Content -->
	<div class="container body">
		<div class="row">
			<div class="col-md-8">
			<div class="portfolio-item shadowstore">
				<div class="head text-center">{{ $heading }}</div>
				<ul class="list-group">
					@foreach($posts as $news)
					<li class="list-group-item">
						<h4><a href="{{ route('view_post', $news[TITLE_URL]) }}"
							>{{((strlen($news[TITLE]) > $num) ? (substr($news[TITLE], 0, $num) . '...') : $news[TITLE])}}</a></h4>
					</li>
					@endforeach
				</ul>
		<div class="text_reading_format" style="clear:left;">
			<br />
			To Practice JAMB 2017 CBT online for free - <a href="">Click Here</a>
			<br /> <br />
			To Download CBT Mobile App - <a href="">Click Here</a>
			<br /><br />
			To Download CBT Software for Computer Systems - <a href="" target="_blank">Click Here</a>
		</div>
		     
			</div>
			</div>
         <div class="col-md-4">
         	<div class="shadowsideba">
				@include('sidebar.view_sub_sidebar')
         	</div>
		 </div>
		</div>		
	</div>

@stop
