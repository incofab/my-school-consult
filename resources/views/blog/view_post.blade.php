<?php
use Illuminate\Support\Facades\Session;

$page = $blogPost[CATEGORY];

?>
@extends('blog.layout')


@section('home_content')

   
<!-- Page Content -->
<div class="container top">

        <div class="row">
            <!-- Blog Post Content Column -->
            <div class="col-lg-8 shadowstore2">

                <!-- Blog Post -->
               <h1>{{$blogPost[TITLE]}}</h1>

                <p class="lead">
                	Posted by <a href="{{ route('view_profile', $blogPost[AUTHOR]) }}">{{$blogPost[AUTHOR]}}</a>
                </p>
                <hr>

                <!-- Date/Time -->
               <p><span class="glyphicon glyphicon-time"></span> Posted on {{$blogPost[CREATED_AT]}}</p>
               <hr>

                <!-- Post Content -->
                <div>
                	<p>{!! $blogPost[CONTENT] !!}</p>
                </div>
                <hr>

				<div class="text_reading_format" style="clear:left;">
				
<br>

 
To Practice JAMB 2017 CBT online for free - <a href="">Click Here</a>
<br>
<br>
To Download CBT Mobile App - <a href="">Click Here</a><br>
<br>
To Download CBT Software for Computer Systems - <a href="" target="_blank">Click Here</a>
</div>
<hr>
 <img class="img-responsive" src="http://placehold.it/900x300" alt="" width="700px">


<br>

                          
              <div class="head"><h2>Comment </h2></div>
              <hr>

   
              <!-- Posted Comments -->
				@foreach($comments as $comment)
                <!-- Comment -->
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                     <h4 class="media-heading">Posted by {{$comment[AUTHOR]}}
                        <small>On &nbsp;&nbsp; <small>{{$comment[CREATED_AT]}}</small></small>
                     </h4>
                     <hr>
                   </div>
                   <div>
                   	<p>{!! $comment[CONTENT] !!}</p>
                   </div>
               	</div>
              	@endforeach
                 <!-- Blog Comments -->
<hr />        
<hr />

                <!-- Comments Form -->
		<div class="well">
	        @if(Session::has(ERROR_MSG))
	        <div class="alert alert-warning">
	        	<p><?= Session::get(ERROR_MSG) ?></p>
	        </div>
	        @endif
            <div id="form-container">
	            <form method="post" id="signup-form" autocomplete="off">
	            
	            	{{ csrf_field() }}
	            
	                <div class="form-group">
	                	<input type="text" value="{{ getValue($post, AUTHOR) }}" name="{{AUTHOR}}" placeholder="Name" class="form-control" >
	                </div>
	                <div class="form-group">
	                	<input type="text" class="form-control" id="name" value="{{ getValue($post, EMAIL) }}" name="{{EMAIL}}" placeholder="Your Mail [optional]" />
	                </div>
	                <div class="form-group">
	                	<textarea class="form-control" name="{{CONTENT}}" id="comment" placeholder="Comment">{{ getValue($post, CONTENT) }}</textarea>
	                </div>
	                <hr />
	                <div class="form-group">
	                	<input type="hidden" value="{{ $blogPost[TABLE_ID] }}" name="{{ POST_ID }}" />
	                	<input type="hidden" name="post_comment" value="true" />
	                	<button class="btn btn-primary">Submit</button>
	                </div>
	            </form>   
			</div>  
		 	<p id="message"></p>
		</div>
		<hr>
	</div>

	<!-- Blog Sidebar Widgets Column -->
	<div class="col-lg-4 shadowsideba">
	@include('sidebar.post_sidebar')
	</div>
		
	</div>
	<!-- /.row -->

</div>
<!-- /.container -->
	
@endsection