<?php $page = 'contact'; ?>
@extends('blog.layout')


@section('home_content')

<!-- Page Content -->
<div class="container body top">

    <div class="row">
        <!-- Blog Post Content Column -->
        <div class="col-lg-8 shadowstore">
           
            <div class="head "> <h2>Contact MyschoolConsult Staff  Directly</h2></div>
             <div class="container ">
             
           <div class="col-sm-6">
               <div class=" panel-default">
                    <div class="panel-heading"><i class="fa fa-contact"></i> Get in touch</div>
                    <div class="panel-body">
                        <form method="post" action="" class="form-horizontal" id="contact_form_id">
                            <div class="form-group">
                                <label class="col-sm-3">Fullname</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" id="name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" name="email" id="email" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3">Phone Number</label>
                                <div class="col-sm-9">
                                    <input type="tel" name="phone" id="phone" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3">Community</label>
                                <div class="col-sm-9">
                                    <select name="community" id="community" class="form-control">
                                        <option value="">-Select-</option>
                                         
                                        </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3">Inquiry</label>
                                <div class="col-sm-9">
                                    <select name="inquiry" id="inquiry" class="form-control">
                                        <option value="">-Select-</option>
                                        <option value="Advert Placement">Advert Placement</option>
                                        <option value="Complain">Complain</option>
                                        <option value="Suggestion">Suggestion</option>
                                        <option value="Observation">Observation</option>
                                        <option value="Commendation">Commendation</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3">Message</label>
                                <div class="col-sm-9">
                                   <textarea class="form-control" name="message" id="comessage"></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-12">
                                   <button id="submit" name="submit" type="button" class="btn btn-success">Send Message</button>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>


                </div>

        


            <div class="text_reading_format" style="clear:left;">
                <br>
                To Practice JAMB 2017 CBT online for free - <a href="">Click Here</a>
                <br>
                <br>
                To Download CBT Mobile App - <a href="">Click Here</a><br>
                <br>
                To Download CBT Software for Computer Systems - <a href="" target="_blank">Click Here</a>
            </div>
            <hr>

        </div>
            

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-lg-4 shadowsideba">
            @include('sidebar.contact_sidebar')
        </div>
        </div>
        
        
 <!-- /.row -->
    <hr>

</div>
<!-- /.container -->

@endsection