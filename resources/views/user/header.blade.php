<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MyschoolConsult</title>


    <!-- Bootstrap Core CSS 
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

     <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
     
    
     <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
     -->
     
    {{Html::style('css/bootstrap.min.css')}}
    
    {{Html::style('font-awesome/css/font-awesome.min.css')}}

    <!-- Custom CSS -->
    {{Html::style('css/dashboard-style.css')}}

    <!-- Morris Charts CSS -->
    {{Html::style('css/plugins/morris.css')}}

   
    {{Html::script('js/jquery.js') }}

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">MyschoolConsult Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
               
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ $data[NAME] }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ url('/logout') }}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="{{route('user_home')}}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#1"> Post news <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="1" class="collapse">
                            <li>
                                <a href="{{ route('create_post') }}">New Post</a>
                            </li>
                            <li>
                                <a href="{{ route('view_my_posts') }}">My Posts</a>
                            </li>
                            <li>
                                <a href="{{ route('view_all_posts') }}">All Posts</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#2"> IReport <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="2" class="collapse">
                             <li>
                                <a href="{{ route('ir_create_post') }}">New Post</a>
                            </li>
                            <li>
                                <a href="{{ route('ir_view_my_posts') }}">My Posts</a>
                            </li>
                            <li>
                                <a href="{{ route('ir_view_all_posts') }}">All Posts</a>
                            </li>
                        </ul>
                    </li>
                     <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#7"> Store <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="7" class="collapse">
                            <li>
                                <a href="{{ route('store_new_product') }}">Add Product</a>
                            </li>
                            <li>
                                <a href="{{ route('store_view_all') }}">View Products</a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>