
  	<hr>

        <!-- Footer -->
        <footer style="background-color: #fefefe">
            <div class="row text-center">
                <div class="col-lg-12">
                	<br />
                    <p>Copyright &copy; Your Website <?= date('Y') ?></p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    
    <!-- Bootstrap Core JavaScript 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    -->
    {{Html::script('js/bootstrap.min.js') }}
       
    
    @if(isset($usesTinyMCE))
    
	    {{Html::script('js/tinymce/js/tinymce/tinymce.min.js') }}
<!-- 	    {{Html::script('vendor/laravel-filemanager/js/lfm.js') }} -->
	    
    	<script>
    	
		  var editor_config = {
		    path_absolute : '/blog/public/',
		    selector: "textarea#editable",
		    height: 300,
		    plugins: [
		      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		      "searchreplace wordcount visualblocks visualchars code fullscreen",
		      "insertdatetime media nonbreaking save table contextmenu directionality",
		      "emoticons template paste textcolor colorpicker textpattern"
		    ],
		    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
		    relative_urls: false,
		    file_browser_callback : function(field_name, url, type, win) {
		      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
		      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
		
		      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
		      if (type == 'image') {
		        cmsURL = cmsURL + "&type=Images";
		      } else {
		        cmsURL = cmsURL + "&type=Files";
		      }
		
		      tinyMCE.activeEditor.windowManager.open({
		        file : cmsURL,
		        title : 'Filemanager',
		        width : x * 0.8,
		        height : y * 0.8,
		        resizable : "yes",
		        close_previous : "no"
		      });
		    }
		  };
		
		  tinymce.init(editor_config);
		  
		</script>
    	
    @endif
    

</body>

</html>