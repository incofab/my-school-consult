@extends('user.layout')


@section('dashboard_content')

<br />

<!-- Page Heading -->
<div class="row">
	<!-- Page Heading -->
	<div class="col-lg-12">
		<h2 class="page-header">
			{{ $header }}
		</h2>
		
		<ol class="breadcrumb">
			<li>
				<i class="fa fa-dashboard"></i>  <a href="{{ route('user_home') }}">Dashboard</a>
			</li>
			<li class="active">
				<i class="fa fa-file"></i> {{ $header }}
			</li>
		</ol>
	</div>
</div>
                <!-- /.row -->
	@include('common.create_post_form')

@endsection
