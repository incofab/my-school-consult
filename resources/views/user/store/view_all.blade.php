@extends('user.layout')


@section('dashboard_content')

<br />

<!-- Page Heading -->
<div class="row">
	<!-- Page Heading -->
	<div class="col-lg-12">
		<h2 class="page-header">
			Products Store
		</h2>
		
		<ol class="breadcrumb">
			<li>
				<i class="fa fa-dashboard"></i>  <a href="{{ route('user_home') }}">Dashboard</a>
			</li>
			<li class="active">
				<i class="fa fa-file"></i> store
			</li>
		</ol>
	</div>
	
	<div> &nbsp;&nbsp;&nbsp;
		<a href="{{ route('store_new_product') }}"><button class="btn btn-primary"
			><i class="fa fa-plus"></i> New</button></a></div>
	
	<br />
	
</div>
<!-- /.row -->


                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th style="width: 1%">S/No</th>
                          <th style="width: 20%">Title</th>
                          <th>Price</th>
                          <th>Description</th>
                          <th style="width: 20%">#Edit</th>
                        </tr>
                      </thead>
                      <tbody>
                      
                      <?php $i = 1; ?>
                      @foreach($posts as $post)
                      
                        <tr>
                          <td>{{$i}}</td>
                          <td>{{ $post[TITLE] 	}}</td>
                          <td>{{ $post[PRICE] 	}}</td>
                          <td>{{ substr($post[DESCRIPTION], 0, 150) . '...' }} </td>
                          
                          <td><?php ?>
                            <a href="{{ route('store_preview_product', [ str_replace(' ', '-', $post[TITLE]) ]) }}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Preview </a>
                            
                            <a href="{{ route('store_edit_product', [$post[TABLE_ID]]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                            <a href="{{ route('store_delete_product', [$post[TABLE_ID]]) }}" class="btn btn-danger btn-xs" 
                            	onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i> Delete </a>
                            
                          </td>
                        </tr>
                        
                      <?php $i++;?>
                      @endforeach
                      
                      </tbody>
                    </table>
                    <!-- end project list -->
	

@endsection
