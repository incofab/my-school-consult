<?php $usesTinyMCE = true; ?>
@extends('user.layout')

@section('dashboard_content')

<br />

<!-- Page Heading -->
<div class="row">
	<!-- Page Heading -->
	<div class="col-lg-12">
		<h2 class="page-header">
			Product Store
		</h2>
		
		<ol class="breadcrumb">
			<li>
				<i class="fa fa-dashboard"></i>  <a href="{{ route('user_home') }}">Dashboard</a>
			</li>
			<li class="active">
				<i class="fa fa-file"></i> Store
			</li>
		</ol>
	</div>
</div>
                <!-- /.row -->
	


<!-- <div class="container"> -->
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">{{ (isset($edit) ? 'Edit Product' : 'Add New Product') }}</div>
                <div class="panel-body">
                
                    <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">
                    
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has(TITLE) ? ' has-error' : '' }}">
                            <label for="" class="col-md-2 control-label">Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="<?= TITLE ?>" value="{{ getValue($post, TITLE) }}" >

                                @if ($errors->has(TITLE))
                                    <span class="help-block">
                                        <strong>{{ $errors->first(TITLE) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
						<div class="form-group{{ $errors->has(CATEGORY) ? ' has-error' : '' }}" >
                            <label for="" class="col-md-2 control-label">Category</label>
                            <div class="col-md-10">
                            	<select name="{{ CATEGORY }}" id="main-category" class="form-control"
                            		required="required" >
                            		<option value="" >select category</option>
                            		<option>{{ CATEGORY_STORE_COMPUTER_SOFTWARE }}</option>
                            		<option>{{ CATEGORY_STORE_MOBILE_APP }}</option>
                            		<option>{{ CATEGORY_STORE_ACTIVATION_PIN }}</option>
                            		<option>{{ CATEGORY_STORE_BOOK }}</option>
                            		<option>{{ CATEGORY_STORE_FORMS }}</option>
                            		<option>{{ CATEGORY_STORE_PROJECT_WORK }}</option>
                            	</select>

                                @if ($errors->has(CATEGORY))
                                    <span class="help-block">
                                        <strong>{{ $errors->first(CATEGORY) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has(DESCRIPTION) ? ' has-error' : '' }}">
                            <label for="" class="col-md-2 control-label">Description</label>
                            <div class="col-md-10">
                            	<textarea name="<?= DESCRIPTION ?>" class="form-control" id="editable">{{ getValue($post, DESCRIPTION) }}</textarea>
                                @if ($errors->has(DESCRIPTION))
                                    <span class="help-block">
                                        <strong>{{ $errors->first(DESCRIPTION) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has(PRICE) ? ' has-error' : '' }}">
                            <label for="" class="col-md-2 control-label">Price</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="<?= PRICE ?>" value="{{ getValue($post, PRICE) }}" >
                                @if ($errors->has(PRICE))
                                    <span class="help-block">
                                        <strong>{{ $errors->first(PRICE) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has(IMAGES) ? ' has-error' : '' }}">
                            <label for="" class="col-md-2 control-label">Image</label>
                            <div class="col-md-10">
                                <input type="file" class="form-control" name="<?= IMAGES ?>" value="{{ getValue($post, IMAGES) }}" >
                                @if ($errors->has(IMAGES))
                                    <span class="help-block">
                                        <strong>{{ $errors->first(IMAGES) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has(TAG) ? ' has-error' : '' }}">
                            <label for="" class="col-md-2 control-label">Tags</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="<?= TAG ?>" value="{{ getValue($post, TAG) }}" >
                                @if ($errors->has(TAG))
                                    <span class="help-block">
                                        <strong>{{ $errors->first(TAG) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                        	
                        	@if(isset($edit))
								<input type="hidden" name="edit_product" value="true" />                        
        	                	<input type="hidden" name="{{ TABLE_ID }}" value="{{ getValue($post, TABLE_ID) }}" />
        	                @else
								<input type="hidden" name="add_new" value="true" />                        
                        	@endif
                        	
                            <div class="col-md-10 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">{{ isset($edit) ? 'Update' : 'Submit'}}</button>
                            </div>
                            
                        </div>
                        
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
<!-- </div> -->



@endsection
