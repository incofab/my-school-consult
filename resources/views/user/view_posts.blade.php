@extends('user.layout')


@section('dashboard_content')

<br />

<!-- Page Heading -->
<div class="row">
	<!-- Page Heading -->
	<div class="col-lg-12">
		<h2 class="page-header">
			{{$header}}
		</h2>
		
		<ol class="breadcrumb">
			<li>
				<i class="fa fa-dashboard"></i>  <a href="{{ route('user_home') }}">Dashboard</a>
			</li>
			<li class="active">
				<i class="fa fa-file"></i> {{$header}}
			</li>
		</ol>
	</div>
	
	<div> &nbsp;&nbsp;&nbsp;
		<a href="{{ route($create_post) }}"><button class="btn btn-primary"
			><i class="fa fa-plus"></i> New</button></a></div>
	
	<br />
	
</div>
<!-- /.row -->


    

                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th style="width: 1%">Id</th>
                          <th style="width: 20%">Title</th>
                          <th>Author</th>
                          <th>Content</th>
                          <th style="width: 20%">#Edit</th>
                        </tr>
                      </thead>
                      <tbody>
                      
                      <?php $i = 1; ?>
                      @foreach($posts as $post)
                      
                        <tr>
                          <td>{{$i}}</td>
                          <td>{{ $post[TITLE] 	}}</td>
                          <td>{{ $post[AUTHOR] 	}} </td>
                          <td>{{ substr($post[CONTENT], 0, 200) . '...' }} </td>
                          
                          <td>
                            <a href="{{ route($preview_post, [ str_replace(' ', '-', $post[TITLE]) ]) }}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Preview </a>
                            @if(Auth::user()[EMAIL] == $post[AUTHOR])
	                            <a href="{{ route($edit_post, [$post[TABLE_ID]]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
	                            <a href="{{ route($delete_post, [$post[TABLE_ID]]) }}" class="btn btn-danger btn-xs" 
	                            	onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i> Delete </a>
                            @endif
                          </td>
                        </tr>
                        
                      <?php $i++;?>
                      @endforeach
                      
                      </tbody>
                    </table>
                    <!-- end project list -->
	

@endsection
