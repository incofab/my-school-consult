

<!-- <div class="container"> -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $heading }}</div>
                <div class="panel-body">
                
                    <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">
                    
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has(TITLE) ? ' has-error' : '' }}">
                            <label for="" class="col-md-2 control-label">Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="<?= TITLE ?>" value="{{ getValue($post, TITLE) }}" >

                                @if ($errors->has(TITLE))
                                    <span class="help-block">
                                        <strong>{{ $errors->first(TITLE) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

						<?php $allCategories = getAllCategories();?>
                        <div class="form-group{{ $errors->has(CATEGORY) ? ' has-error' : '' }}" >
                            <label for="" class="col-md-2 control-label">Category</label>
                            <div class="col-md-10">
                            	<select name="{{ CATEGORY }}" id="main-category" class="form-control"
                            		onchange="categorySelected(this)" required="required" >
                            		<option value="" >select category</option>
                            		@foreach($allCategories as $category)
	                            		<option value="{{$category}}" 
	                            			<?= (getValue($post, CATEGORY) == $category) ? ' selected="selected" ' : '' ?> 
	                            		>{{$category}}</option>
                            		@endforeach
                            	</select>

                                @if ($errors->has(CATEGORY))
                                    <span class="help-block">
                                        <strong>{{ $errors->first(CATEGORY) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has(SUB_CATEGORY) ? ' has-error' : '' }}" >
                            <label for="" class="col-md-2 control-label">Sub Category</label>
                            <div class="col-md-10">
                            	<select name="{{ SUB_CATEGORY }}" id="sub-category" class="form-control" required="required" >
									<option value="">---</option>
                            	</select>

                                @if ($errors->has(SUB_CATEGORY))
                                    <span class="help-block">
                                        <strong>{{ $errors->first(SUB_CATEGORY) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has(SHORT_DESC) ? ' has-error' : '' }}">
                            <label for="" class="col-md-2 control-label">Short Description</label>
                            <div class="col-md-10">
                            	<textarea name="<?= SHORT_DESC ?>" class="form-control" cols="30" rows="2" >{{getValue($post, SHORT_DESC)}}</textarea>
                                @if ($errors->has(SHORT_DESC))
                                    <span class="help-block">
                                        <strong>{{ $errors->first(SHORT_DESC) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has(CONTENT) ? ' has-error' : '' }}">
                            <label for="" class="col-md-2 control-label">Content</label>
                            <div class="col-md-10">
                            	<textarea name="<?= CONTENT ?>" class="form-control" id="editable" >{{ getValue($post, CONTENT) }}</textarea>
                                @if ($errors->has(CONTENT))
                                    <span class="help-block">
                                        <strong>{{ $errors->first(CONTENT) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has(TAG) ? ' has-error' : '' }}">
                            <label for="" class="col-md-2 control-label">Tags</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="<?= TAG ?>" value="{{ getValue($post, TAG) }}" >
                                @if ($errors->has(TAG))
                                    <span class="help-block">
                                        <strong>{{ $errors->first(TAG) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has(IMAGES) ? ' has-error' : '' }}">
                            <label for="" class="col-md-2 control-label">Image</label>
                            <div class="col-md-10">
                                <input type="file" name="<?= IMAGES ?>" value="{{ getValue($post, IMAGES) }}" >
                                @if ($errors->has(IMAGES))
                                    <span class="help-block">
                                        <strong>{{ $errors->first(IMAGES) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                        	
                        	@if(isset($edit))
								<input type="hidden" name="edit_post" value="true" />                        
        	                	<input type="hidden" name="{{ TABLE_ID }}" value="{{ getValue($post, TABLE_ID) }}" />
        	                @else
								<input type="hidden" name="create_new" value="true" />                        
                        	@endif
                        	
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">{{ isset($edit) ? 'Update' : 'Submit'}}</button>
                            </div>
                            
                        </div>
                        
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
<!-- </div> -->

<script type="text/javascript">

	var categories = <?= json_encode(getAllSubCategories()) ?>; 
// 		{
			
// 		Scholarships : [	
// 			'Undergraduate Scholarship',
// 			'Graduate Scholarship',
// 			'International Scholarship',
// 			'Postgraduate Scholarship',
// 		],
// 		"General News" : [
// 			'Latest News', 'University IReport',
// 			'Latest Jamb News', 'Gist',
// 		],
// 		NYSC : [
// 			'NYSC News'
// 		],
// 		Internship : [
// 			'Industrial Training', 
// 			'Undergraduate Internship',
// 			'Graduate Internship'
// 		],
		
// 	}

	categorySelected('#main-category');
		
	function categorySelected(obj) {
		
		var selected = $(obj).val();
		if(!selected) return;
		
		var subCategories = categories[selected];
		
		if(!subCategories)return alert('Invalid category selected = ' + selected );
		
		var s = '';
		subCategories.forEach(function(sub, i) {
			s += '<option >' + sub + '</option>';
		});
		
		$('#sub-category').html(s);
	}
	
</script>

<style>
	select option{
		padding: 5px 3px;
	}
</style>