
<style type="text/css">
	.lim td{
		width: 90px
	}
	.table_le {
		table-layout: fixed;
	}
	.table_le td {
		word-wrap: break-word;
	}

</style>

<div >

	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">Users</h3>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="panel-body">
			<div class="">
				<table width="100%" class="table table-striped table-bordered table_le" style="font-size: 12px;padding: 2px !important" id="dataTables-example">
					<thead>
						<tr>
							<th>S/No</th>
							<th>Name</th>
							<th>Phone</th>
							<th>Email</th>
							<th>Address</th>
							<th>Status</th>
							<th>Operation</th>
						</tr>
					</thead>
				<tbody>
					<?php $i = 1; ?>
					@foreach($allRecords as $user)
						<tr class='odd gradeX lim'>
							<td style="width: 20px">{{ $i }}</td>
							<td style="width: 100px">{{ $user[FULLNAME] }}</td>
							<td style="width: 40px">{{ $user[PHONE_NO] }}</td>
							<td style="width: 40px">{{ $user[EMAIL] }}</td>
							<td style="width: 40px">{{ $user[ADDRESS] }}</td>
							<td style="width: 40px">{{ getUserStatus($user) }}</td>
							
							@if(isset($isAdmin))
							<td>
								<a href="<?= getAddr('admin_view_user_profile', $user[TABLE_ID]) ?>" style='margin-left:5px'><i class='fa fa-user fa-fw'></i> Profile</a>
								<br />
								@if($user[SUSPENDED])
									<a href="<?= getAddr('admin_user_unsuspend', $user[TABLE_ID]) ?>" 
										style='margin-left:5px' onclick="return confirm('Un-Suspend User?')" 
										><i class='fa fa-user fa-fw'></i> <small>Un-Suspend</small></a>
									<br />
								@else
									<a href="<?= getAddr('admin_user_suspend', $user[TABLE_ID]) ?>" 
										style='margin-left:5px' onclick="return confirm('Suspend User?')" 
										><i class='fa fa-user fa-fw'></i> Suspend</a>
									<br />
								@endif
								<a href="<?= getAddr('admin_delete_user', $user[TABLE_ID]) ?>" 
									style='margin-left:5px' onclick="return confirm('Are you sure?')" 
									><i class='fa fa-trash fa-fw'></i> Delete</a>
							</td>
							@endif
							@if(isset($isAgent))
							<td>
								<a href="<?= getAddr('agent_view_user_profile', $user[TABLE_ID]) ?>" style='margin-left:5px'><i class='fa fa-user fa-fw'></i> Profile</a>
								<a href="<?= getAddr('agent_renew_user_sub', $user[TABLE_ID]) ?>" style='margin-left:5px'><i class='fa fa-money fa-fw'></i> Renew</a>
							</td>
							@endif
							
						</tr>
					<?php $i++; ?>
					@endforeach
				</tbody>
				</table>
			</div>
		<!-- /.table-responsive -->
		</div>
	</div>

	 


<!-- /.row -->
</div>
<!-- /#page-wrapper -->
<?php 

?>