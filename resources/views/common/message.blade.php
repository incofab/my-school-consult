<?php


use Illuminate\Support\Facades\Session;
/**
 * Used to include neutral messages into another page
 * NOTE: The function controlling this page resides in the page footer 
 */

$error = isset($error) ? $error : Session::get(ERROR_MSG);
$report = isset($report) ? $report : Session::get(WARNING_MSG);
$success = isset($success) ? $success : Session::get(SUCCESS_MSG);

// $error = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est neque.';
?>
	
<div id="error-msgs">
	
	@if(!empty($error))
			<p class="error"><?= $error ?></p><br />
	@endif
	@if(!empty($report))
			<p class="report"><?= $report ?></p><br />
	@endif
	@if(!empty($success))
			<p class="success"><?= $success ?></p><br />
	@endif
	
</div>

	
<style>

#error-msgs{
	position: fixed;
	top: 50px;
	left: 0;
	right: 0;
	width: 0;
	z-index: 99999;
	overflow: hidden;
	margin: auto;
}

/* The colors here is generic... All color style share the same (except the report) Error and highlight */
.report, .report_e, .report_s, .success, .error, .errorMsg{
	position: relative;
	display: block;
	font-weight: bold;
	padding: 8px 5px;
	text-align: center;
	font-size: 13px;
	margin: 5px 0;
}
.report_e, .error, .errorMsg{
	background: #c41515;
	color: white;
}
.report_s, .success{
	background: #008000;
	color: white;
}
.report{
	background: #af6a03;
	color: white;
}
.report:AFTER, .report_e:AFTER, .report_e:BEFORE, .report_s:AFTER, .report_s:BEFORE,
.success:AFTER, .error:AFTER, .errorMsg:AFTER,
.success:BEFORE, .error:BEFORE, .errorMsg:BEFORE {
	position: absolute;
	display: block;
	color: white;
	font-weight: bold;
	top: 0;
	left: 0;
	margin: 4px 0 0 10px;
}
.report:AFTER{
	content: 'i';
	background: #af6a03;
	padding: 0 8px 15px 5px;
	border-radius: 50%;
	height: 8px;
	width: 8px;
	font-size: 14px;
	font-weight: 1000;
}
.report_e:AFTER, .error:AFTER, .errorMsg:AFTER {
	content: '';
	border-bottom: solid 18px #eb4141;
	border-left: solid 9px transparent;
	border-right: solid 12px transparent;
	z-index: 1;	 
}
.report_e:BEFORE, .error:BEFORE, .errorMsg:BEFORE {
	content: '!';
	height: 15px;
	width: 15px;
	z-index: 2;
	font-size: 11px;
	margin: 8px 0 0 12px;
}
.report_s:AFTER, .success:AFTER {
	content: '';
	border-left: solid 3px white;
	border-bottom: solid 3px white;
	width: 12px;
	height: 6px;
	background: transparent;
	transform: rotate(-45deg);
	z-index: 2;
	margin: 9px 0 0 14px;
}
.report_s:BEFORE, .success:BEFORE {
	content: '';
	background: green;
	width: 20px;
	height: 20px;
	z-index: 1;
	border-radius: 50%;
}

/* End of Error and highlight */

	
</style>
	
