<?php 
$errors = isset($errors) ? $errors : [];
$post = isset($post) ? $post : [];

?>

<style>
	#reg_email{
 		width: 70%;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        box-shadow: inset 0 1px 3px #ddd;
        border-radius: 4px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding-left: 20px;
        padding-right: 20px;
        padding-top: 12px;
        padding-bottom: 12px; 		
	}
</style>

	<div class="row">
	
	<Center>
		
		<div class="col-md-12" style="background: rgba(225,225,225,0.6);width: 95%; float: none; padding: 10px">

		@if(!isset($edit))
			<h1 style="text-align: center;font-weight: 700">
				Create an account today
			</h1>
		@endif
			
			<h4 style="text-align: center; font-weight: lighter;"></h4>
			
			<div style="width: 85%">
			
					<?php showFormValidationErrors($errors); ?>
					
					<div class="loginwr">
						 
							<form autocomplete="off" method="POST" action="" name="register">
								
							<label for="">Full Name</label>
							<input type="text" name="<?= FULLNAME ?>" value="<?= getValue($post, FULLNAME)  ?>" placeholder="Full name" required="required" />
							<br><br />
							
							<label for="">Username</label>
							<input type="text" name="<?= USERNAME?>" value="<?= getValue($post, USERNAME)  ?>" placeholder="Your desired username" required/>
							<br><br />
							
							@if(!isset($edit))
								
								<label for="">Password</label>
								<input type="Password" name="<?= PASSWORD?>" placeholder="Your desired password" required="required"/>
								<br><br />
								
								<label for="">Confirm Password</label>
								<input type="Password" name="<?= PASSWORD_CONFIRMATION?>" placeholder="Confirm password" required="required"/>
								<br><br />
								
								<label for="">Email</label>
								<input type="email" id="reg_email" name="<?= EMAIL?>" value="<?= getValue($post, EMAIL)  ?>" placeholder="Your working email address" required/>
								<br><br />
								
							@endif
							
							
							<label for="">Phone number</label>
							<input type="tel" name="<?= PHONE_NO ?>" value="<?= getValue($post, PHONE_NO)  ?>" placeholder="Your working phone number" required>
							<br>
							<br />
							
							<label for="">Whatsapp number</label>
							<input type="tel" name="<?= WHATSAPP_NO ?>" value="<?= getValue($post, WHATSAPP_NO)  ?>" placeholder="Your working phone number" >
							<br>
							<br />
							
							<label for="">Address</label>
							<input type="text" name="<?= ADDRESS ?>" value="<?= getValue($post, ADDRESS)  ?>" placeholder="Your home address ">
							<br>
							<br />
							
							<label for="">City</label>
							<input type="text" name="<?= CITY ?>" value="<?= getValue($post, CITY)  ?>" placeholder="Your city">
							<br>
							<br />
							
							<label for="">Gender</label>
							<select name="<?= GENDER ?>" required="required" >
	                           <option value="male" <?= (getValue($post, GENDER) == 'male') ? ' selected=""selected ' : ''  ?>>Male</option>
	                           <option value="female" <?= (getValue($post, GENDER) == 'female') ? ' selected=""selected ' : ''  ?>>Female</option>
                       		</select>
							<br /><br />
							
							<label for="">Country</label>
							<select name="<?= COUNTRY ?>">
								@include('common.countries')   
							</select>
							<br>
							<br />
							
							@if(isset($edit))
								<input type="hidden" name="<?= TABLE_ID ?>" value="<?= getValue($post, TABLE_ID)  ?>" />
								<input type="hidden" name="edit_profile" value="true" />
							@else
								<input type="hidden" name="register_user" value="true" />
							@endif 
							<input type="submit"  name="register" style="width: 70%" class="btn btn-primary btn-block" value="Register">

							</form>
							<br>
				@if(!isset($edit))
					</div>
					<p>Do you already have an account? <strong> <a href="<?= getAddr('user_login')?>" style="color: black">Click here to login</a> </strong> </p>
					</div>
				@endif

			<div class="clearfix"></div>
			
	</div>
</Center>
</div>