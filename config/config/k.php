<?php

// ini_set( "display_errors" , true );


/**
 * Checks if key exists in the given array
 * @param array $post
 * @param string $key
 * @return mixed the value if true, returns default value otherwise
 */
function getValue($post, $key, $default = NULL) {
	return (isset($post[$key]) ? $post[$key] : $default);
}

function getAllCategories() {
	return [CATEGORY_GENERAL, CATEGORY_INTERNSHIP, CATEGORY_NYSC, CATEGORY_SCHOLARSHIP];
}

function getAllStoreCategories() {
	return [CATEGORY_STORE_ACTIVATION_PIN, CATEGORY_STORE_BOOK, CATEGORY_STORE_COMPUTER_SOFTWARE,
			CATEGORY_STORE_FORMS, CATEGORY_STORE_MOBILE_APP, CATEGORY_STORE_PROJECT_WORK];
}

function getAllSubCategories() {
	return [
			CATEGORY_GENERAL => [
					SUB_CAT_LATEST_NEWS, SUB_CAT_UNIVERSITY_IREPORT,
					SUB_CAT_ADMISSION_NEWS, SUB_CAT_POST_UTME_NEWS,
					SUB_CAT_JAMB_NEWS, SUB_CAT_GIST, 
				],	
			CATEGORY_SCHOLARSHIP => [	
					SUB_CAT_UNDERGRADUATE_SCHOLARSHIP,
					SUB_CAT_GRADUATE_SCHOLARSHIP,
					SUB_CAT_INTERNATIONAL_SCHOLARSHIP,
					SUB_CAT_POST_SCHOLARSHIP,
				],
			CATEGORY_INTERNSHIP => [
					SUB_CAT_INDUSTRIAL_TRAINING, 
					SUB_CAT_UNDERGRADUATE_INTERNSHIP,
					SUB_CAT_GRADUATE_INTERNSHIP
				], 
			CATEGORY_NYSC => [
					SUB_CAT_NYSC_NEWS
				], 
	];
}

function getCategoryIndex($category) {
	$cats = getAllCategories();
	for ($i = 0; $i < count($cats); $i++) {
		if($cats[$i] === $category) return $i;
	}
	return 0;
}

function errorhandling() {
	ini_set('display_errors', 'Off');
	ini_set('display_start_up_errors', 'Off');
	error_reporting(E_ERROR);
	//set_error_handler($error_handler)
}


function dDie($param) {
	echo '<pre>'; print_r($param); die();
}
	