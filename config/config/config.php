<?php
// require_once 'inst.php';

/* Installation constants */
	
define("DB_HOSTNAME", "localhost");
define("DB_USERNAME", "cbt");
define("ACCT_USERNAME", "");// fnet_17890297 The username of the hosting platform (Can be equal to DB_USERNAME plus _ )
define("DB_PASSWORD", "");

define("ROOT_FOLDER", "/blog/");

// //Databases
define("DB_NAME", 'blog');

// Tables
define("POSTS_TABLE", 'posts');
define("POST_COMMENTS_TABLE", 'post_comments');
define("IREPORTS_TABLE", 'ireports');
define("IREPORTS_COMMENTS_TABLE", 'ireport_comments');
define("STORE_TABLE", 'store');
define("USERS_TABLE", 'users');
// define("CATEGORIES_TABLE", 'categories');
// define("SUB_CATEGORIES_TABLE", 'sub_categories');
// /**	Serves as a pivot table joining the post, it'scategories and sub category */
// define("POST_CATEGORIES_TABLE", 'post_categories');
// /**	Serves as a pivot table joining the post and it tags */
// define("POST_TAGS_TABLE", 'post_tags');
// define("TAGS_TABLE", 'tags');


/*// End of Installation Constants */

define("K_NEWLINE", PHP_EOL);
define("CSRF_TOKEN", '_token');

//TAble names
define("TITLE", "title");
define("TITLE_URL", "title_url");
define("IMAGES", "image");
define("CONTENT", "content");
define("AUTHOR", "author");
define("SHORT_DESC", "short_desc");
define("IS_APPROVED", "is_approved");
define("IS_READ", "is_read");
define("IS_SUSPENDED", "is_suspended");
define("TAG", "tag");
define("DESCRIPTION", "description");
define("PRICE", "price");

// define("SCHOLARSH", "scholarship");


define("NAME", "name");
define("USERNAME", "username");
define("PASSWORD", "password");
define("ERROR_MSG", "error_msg");
define("SUCCESS_MSG", "success_msg");
define("WARNING_MSG", "warning_msg");






define("SURNAME", "surname");
define("FIRSTNAME", "firstname");
define("OTHERNAMES", "othernames");
define("PHONE_NO", "phone");
define("PASSPORT", "passport");



define("TABLE_ID", "id");
define("POST_ID", "post_id");
define("DOB", "dob");
define("GENDER", "gender");
define("ADDRESS", "address");
define("EMAIL", "email");
define("CREATED_AT", "created_at");
define("UPDATED_AT", "updated_at");
define("CREATED_BY", "created_by");
define("UPDATED_BY", "updated_by");
define("SOO", "soo"); //State of origin
define("LGA", "lga");
define("NATIONALITY", "nationality");
define("RELIGION", "religion");

define("DATETIME", "date_time");
define("DATE", "date");
define("TIME", "time");




// Messaging
define("MESSAGE_ID", "messsage_id");
define("MESSAGE_RECEIVER", "receiver");
define("MESSAGE_SENDER", "sender");
define("MESSAGE_BODY", "body");

/**
 * from pagination
 * @var unknown
 */
define("PAGE_NUM", "pageNum");

define("ERRORCODE", "error_code");
define("MESSAGE", "message");
define("SUCCESS", "success");

//Used to specify the url a user will be redirected to after log in
define("REDIRECT_TO", "redirect_to");

// Categories
define("CATEGORY", "category");
define("CATEGORY_SCHOLARSHIP", "Scholarships");
define("CATEGORY_GENERAL", "General News");
define("CATEGORY_NYSC", "NYSC");
define("CATEGORY_INTERNSHIP", "Internship");
define("SUB_CATEGORY", "sub_category");

// Products category
define("CATEGORY_STORE_COMPUTER_SOFTWARE", "computer software");
define("CATEGORY_STORE_MOBILE_APP", "mobile app");
define("CATEGORY_STORE_ACTIVATION_PIN", "activation pin");
define("CATEGORY_STORE_BOOK", "book");
define("CATEGORY_STORE_FORMS", "forms");
define("CATEGORY_STORE_PROJECT_WORK", "project research and work");

// Sub Categories
define("SUB_CAT_UNDERGRADUATE_SCHOLARSHIP", "Undergraduate Scholarship");
define("SUB_CAT_GRADUATE_SCHOLARSHIP", "Graduate Scholarship");
define("SUB_CAT_INTERNATIONAL_SCHOLARSHIP", "International Scholarship");
define("SUB_CAT_POST_SCHOLARSHIP", "Postgraduate Scholarship");
define("SUB_CAT_POST_UTME_NEWS", "Post UTME");
define("SUB_CAT_LATEST_NEWS", "Latest News");
define("SUB_CAT_UNIVERSITY_IREPORT", "University IReport");
define("SUB_CAT_ADMISSION_NEWS", "Admission News");
define("SUB_CAT_JAMB_NEWS", "Latest Jamb News");
define("SUB_CAT_GIST", "Gist");
define("SUB_CAT_NYSC_NEWS", "NYSC News");
define("SUB_CAT_INDUSTRIAL_TRAINING", "Industrial Training");
define("SUB_CAT_UNDERGRADUATE_INTERNSHIP", "Undergraduate Internship");
define("SUB_CAT_GRADUATE_INTERNSHIP", "Graduate Internship");











