<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIreportCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	\App\Models\PostsBaseModel::createPostCommentsTable(IREPORTS_COMMENTS_TABLE, IREPORTS_TABLE);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::drrop(IREPORTS_COMMENTS_TABLE);
    }
}
