<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(USERS_TABLE, function (Blueprint $table) {
            $table->increments(TABLE_ID);
            $table->string(NAME);
            $table->string(EMAIL, 50)->unique();
            $table ->boolean(IS_SUSPENDED)->default(false);
            $table->string(PASSWORD);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(USERS_TABLE);
    }
}
