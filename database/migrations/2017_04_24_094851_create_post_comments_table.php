<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	\App\Models\PostsBaseModel::createPostCommentsTable(POST_COMMENTS_TABLE, POSTS_TABLE);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::drrop(POST_COMMENTS_TABLE);
    }
}
