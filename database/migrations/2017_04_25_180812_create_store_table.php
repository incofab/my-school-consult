<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create(STORE_TABLE, function(Blueprint $table){
    		$table->increments(TABLE_ID);
    		$table ->string(TITLE, 200)->unique();
    		$table ->string(TITLE_URL)->unique();
    		$table ->text(IMAGES)->nullable(true);
    		$table ->text(DESCRIPTION);
    		$table ->string(CATEGORY, 40);
    		$table ->string(PRICE, 15);
    		$table ->string(TAG)->nullable(true);
    		$table->timestamps();    	    		 
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::drrop(STORE_TABLE);
    }
}
