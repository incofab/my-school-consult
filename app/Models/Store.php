<?php

namespace App\Models;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;

class Store extends BaseModel {
	
    public $table = STORE_TABLE;
    protected $fillable = array(TITLE, IMAGES, DESCRIPTION, CATEGORY, PRICE, TAG, TITLE_URL);

    protected $rules_insert = [
    		TITLE 	=> 'required|min:3|max:255',
    		IMAGES  => 'max:500000|mimes:jpg,png,jpeg', //max size approx 500kb
    		PRICE	=> 'required',
    		CATEGORY	=> 'required',
    		DESCRIPTION	=> 'required',
    ];
    
    protected $rules_update = [
    		TITLE  => 'required|min:3|max:255',
    		IMAGES => 'max:500000|mimes:jpg,png,jpeg', //max size approx 500kb
    		PRICE	=> 'required',
    		CATEGORY	=> 'required',
    		DESCRIPTION => 'required',
    ];
     
    
    function addNewProduct($post) {
    		
    	$val = validator($post, $this->rules_insert);
    
    	if ($val->fails()){
    		redirect()->to(URL::current())->withErrors($val)->with('post', $post)->send();
    	}
    	
    	//If it passes, Then check for the duplicate title
    	$ret = $this->where(TITLE, '=', $post[TITLE])->first();
    	
    	if ($ret){
    		redirect()->to(URL::current())
	    		->withErrors([TITLE => 'This title already exists'])
	    		->with('post', $post)->send();
    	}
    	
    	// move uploaded file
    	$filename = ''; 
    	//SInc reg no is valid, Move passport
    	if (Request::file(IMAGES)){
    		$ext = Request::file(IMAGES)->guessExtension();
    		$filename = uniqid('product_') . '.' . $ext;
    		Request::file(IMAGES)->move(URL::asset('/photos/shares/store/'), $filename);
    	}
    	
    	$newRet = $this->create([
    			TITLE 	=> $post[TITLE],
    			TITLE_URL 	=> static::createURLFromTitle($post[TITLE]),
    			PRICE 	=> $post[PRICE],
    			IMAGES 	=> $filename,
    			CATEGORY 	=> $post[CATEGORY],
    			DESCRIPTION => $post[DESCRIPTION],
    			TAG 		=> getValue($post, TAG),
    	]);
    	
    	redirect()->to(URL::current())->with(SUCCESS_MSG, 'Data recorded successfully')->send();
    
    }
    
    function updateProduct($post) {
    		
    	$val = validator($post, $this->rules_update);
    		
    	if ($val->fails()){
    		redirect()->to(URL::current())->withErrors($val)->with('post', $post)->send();
    	}
    
    	//If it passes, Then check for the duplicate title
    	$ret = $this->where(TITLE, '=', $post[TITLE])
    		->where(TABLE_ID, '!=', $post[TABLE_ID])->first();
    
    	if ($ret){
    		redirect()->to(URL::current())
	    		->withErrors([TITLE => 'This title already exists'])
	    		->with('post', $post)->send();
    	}
    	
    	$old = $this->where(TABLE_ID, '=', $post[TABLE_ID])->first();
    	
    	// move uploaded file
    	$filename = null;
    	if (Request::file(IMAGES)){
    		// Delete old pic
			unlink(URL::asset('/photos/shares/store/') . $old[IMAGES]);    		
    		$ext = Request::file(IMAGES)->guessExtension();
    		$filename = uniqid('product_') . '.' . $ext;
    		Request::file(IMAGES)->move(URL::asset('/photos/shares/store/'), $filename);
    	}
    	
    	$success = $this->where(TABLE_ID, '=', $post[TABLE_ID])
	    	->update([
	    			TITLE 	=> $post[TITLE],
	    			TITLE_URL 	=> static::createURLFromTitle($post[TITLE]),
	    			IMAGES 	=> getValue($post, IMAGES, $old[IMAGES]),
	    			PRICE 	=> $post[PRICE],
	    			TAG 	=> getValue($post, TAG),
	    			DESCRIPTION => $post[DESCRIPTION],
	    	]);
    
    	if 	 ($success) redirect()->to(URL::current())->with(SUCCESS_MSG, 'Data updated successfully')->send();
    	else redirect()->to(URL::current())->with([ERROR_MSG, 'post'], ['Update failed', $post])->send();
    		
    }

    
}

