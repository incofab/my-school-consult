<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model{
    
	static function createURLFromTitle($title) {
		$title = str_replace(' ', '-', $title);
		$title = str_replace('/', '_', $title);
		$title = str_replace('?', '', $title);
		$title = str_replace('&', '.', $title);
		return $title . '-' . uniqid();
	}
    
}
