<?php

namespace App\Models;

class IReportComments extends PostsBaseModel{
	
	public $table = IREPORTS_COMMENTS_TABLE;
	
	protected $fillable = array(TITLE, AUTHOR, CONTENT, EMAIL, POST_ID, TAG);
	
	function createNewPostComment($post) {
		\App\Models\PostsBaseModel::createNewPostComment_($this, $post);
	}
	
	function updatePostComment($post) {
		\App\Models\PostsBaseModel::updatePostComment_($this, $post);
	}
	
	function iReport() {
		return $this->belongsTo(\App\Models\IReport::class, POST_ID, TABLE_ID);
	}

}
