<?php
namespace App\Models;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;


class PostsBaseModel extends BaseModel {
	
    protected $rules_insert = [
    		TITLE 	=> 'required|min:3|max:255',
    		CONTENT 	=> 'required',
    		CATEGORY 	=> 'required',
    		SUB_CATEGORY 	=> 'required',
    		IMAGES => 'max:500000|mimes:jpg,png,jpeg' //max size approx 500kb
    ];
    
    protected $rules_update = [
    		TITLE 	=> 'required|min:3|max:255',
    		CONTENT 	=> 'required',
    		CATEGORY	=> 'required',
    		SUB_CATEGORY	=> 'required',
    		IMAGES => 'max:500000|mimes:jpg,png,jpeg' //max size approx 500kb
    ];
    
    protected $comments_rules_insert = [
    		CONTENT 	=> 'required',
    		POST_ID 	=> 'required',
    ];
    
    protected $comments_rules_update = [
    		CONTENT 	=> 'required',
    		POST_ID	=> 'required',
    ];
    

    static function createNewPost_($obj, $post) {
    		
    	$val = validator($post, $obj->rules_insert);
    
    	if ($val->fails()){
    		redirect()->to(URL::current())->withErrors($val)->with('post', $post)->send();
    	}
    		
    	//If it passes, Then check for the duplicate title
    	$ret = $obj->where(TITLE, '=', $post[TITLE])->first();
    		
    	if ($ret){
    		redirect()->to(URL::current())
    		->withErrors([TITLE => 'This title already exists'])
    		->with('post', $post)->send();
    	}
    	
    	// move uploaded file
    	$filename = null; 
    	//SInc reg no is valid, Move passport
    	if (Request::file(IMAGES)){
    		$ext = Request::file(IMAGES)->guessExtension();
    		$filename = uniqid('post_') . '.' . $ext;
    		Request::file(IMAGES)->move(storage_path() . "/app/public/images/{$obj->table}", $filename);
    	}
    	
    	$newRet = $obj->create([
    			TITLE 	=> $post[TITLE],
    			TITLE_URL 	=> static::createURLFromTitle($post[TITLE]),
    			IMAGES 	=> $filename,
    			CONTENT => $post[CONTENT],
    			CATEGORY => $post[CATEGORY],
    			SUB_CATEGORY => $post[SUB_CATEGORY],
    			AUTHOR 	=> Auth::user()[EMAIL],
    			SHORT_DESC => getValue($post, SHORT_DESC),
    			TAG 	=> getValue($post, TAG),
    	]);
    	
    	redirect()->to(URL::current())->with(SUCCESS_MSG, 'Data recorded successfully')->send();
    	
    }
    
    
    static function updatePost_($obj, $post) {
    		
    	$val = validator($post, $obj->rules_update);
    		
    	if ($val->fails()){
    		redirect()->to(URL::current())->withErrors($val)->with('post', $post)->send();
    	}
    
    	//If it passes, Then check for the duplicate title
    	$ret = $obj->where(TITLE, '=', $post[TITLE])
    	->where(TABLE_ID, '!=', $post[TABLE_ID])->first();
    
    	if ($ret){
    		redirect()->to(URL::current())
    		->withErrors([TITLE => 'This title already exists'])
    		->with('post', $post)->send();
    	}
    
    
    	$success = $obj->where(AUTHOR, '=', Auth::user()[EMAIL])
	    	->where(TABLE_ID, '=', $post[TABLE_ID])
	    	->update([
	    			TITLE 	=> $post[TITLE],
	    			TITLE_URL 	=> static::createURLFromTitle($post[TITLE]),
// 	    			IMAGES 	=> getValue($post, IMAGES),
	    			CONTENT => $post[CONTENT],
	    			CATEGORY => $post[CATEGORY],
	    			SUB_CATEGORY => $post[SUB_CATEGORY],
	    			SHORT_DESC => getValue($post, SHORT_DESC),
	    			TAG 	=> getValue($post, TAG),
	    	]);
    
    	if 	 ($success) redirect()->to(URL::current())->with(SUCCESS_MSG, 'Data updated successfully')->send();
    	else redirect()->to(URL::current())->with([ERROR_MSG, 'post'], ['Update failed', $post])->send();
    		
    }
    
    
    
	// Comments // Comments  // Comments 
	
    static function createNewPostComment_($obj, $post) {
    	
    	$val = validator($post, $obj->comments_rules_insert);
    
    	if ($val->fails()){
    		redirect()->to(URL::current())->withErrors($val)->with('post', $post)->send();
    	}
    	 
    	$newRet = $obj->create([
    			AUTHOR 	=> getValue($post, AUTHOR, 'Annonymous'),
    			EMAIL 	=> getValue($post, EMAIL),
    			CONTENT => $post[CONTENT],
    			POST_ID => $post[POST_ID],
    	]);
    	 
    	redirect()->to(URL::current())->with(SUCCESS_MSG, 'Comment recorded successfully')->send();
    
    }

    static function updatePostComment_($obj, $post) {
    
    	$val = validator($post, $obj->rules_update);
    
    	if ($val->fails()){
    		redirect()->to(URL::current())->withErrors($val)->with('post', $post)->send();
    	}
    
    	//If it passes, Then check for the duplicate title
    	$ret = $obj->where(TABLE_ID, '=', $post[TABLE_ID])->first();
    
    	if (!$ret){
    		redirect()->to(URL::current())
	    		->withErrors([TITLE => 'Unknown post'])
	    			->with('post', $post)->send();
    	}
    
    
    	$success = $obj->where(AUTHOR, '=', Auth::user()[EMAIL])
    	->where(TABLE_ID, '=', $post[TABLE_ID])
    	->update([
    			AUTHOR 	=> getValue($post, AUTHOR, 'Annonymous'),
    			EMAIL 	=> getValue($post, EMAIL),
    			CONTENT => $post[CONTENT],
    			POST_ID => $post[POST_ID],
    	]);
    
    	if 	 ($success) redirect()->to(URL::current())->with(SUCCESS_MSG, 'Data updated successfully')->send();
    	
    	else redirect()->to(URL::current())->with([ERROR_MSG, 'post'], ['Update failed', $post])->send();
    
    }
    
    
    
    
    
    
    
    
    // Helper function for migration
    // Helper function for migration

    static function createPostsTable($tableName) {
    
    	Schema::create($tableName, function(Blueprint $table){
    		$table->increments(TABLE_ID);
    		$table ->string(TITLE, 200)->unique();
    		$table ->string(TITLE_URL)->unique();
    		$table ->text(IMAGES)->nullable(true);
    		$table ->text(CONTENT);
    		$table ->string(AUTHOR, 50);
    		$table ->boolean(IS_APPROVED)->default(false);
    		$table ->boolean(IS_READ)->default(false);
    		$table ->text(SHORT_DESC)->nullable(true);
    		$table ->string(TAG)->nullable(true);
    		$table ->enum(CATEGORY, getAllCategories());
    		$table ->string(SUB_CATEGORY, 40);
    		$table->timestamps();
    
    		$table->foreign(AUTHOR)->references(EMAIL)->on(USERS_TABLE)
    			->onDelete('cascade')->onUpdate('cascade');
    		 
    	});
    }
    
    static function createPostCommentsTable($commentTableName, $postTableName) {
    
    	Schema::create($commentTableName, function(Blueprint $table)
    			use ($postTableName){
    					
    				$table->increments(TABLE_ID);
    				$table->integer(POST_ID, false, true);
    				$table->string(AUTHOR, 100)->nullable(true);
    				$table->string(EMAIL, 50)->nullable(true);
    				$table->text(CONTENT);
    				$table->timestamps();
    					
    				$table->foreign(POST_ID)->references(TABLE_ID)->on($postTableName)
    					->onDelete('cascade')->onUpdate('restrict');
    					
    	});
    }
    
    
}

