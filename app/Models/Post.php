<?php

namespace App\Models;

class Post extends PostsBaseModel {
	
    public $table = POSTS_TABLE;
    protected $fillable = array(TITLE, IMAGES, CONTENT, AUTHOR, SHORT_DESC, TAG, CATEGORY, SUB_CATEGORY, TITLE_URL);
    
    function createNewPost($post) {
    	\App\Models\PostsBaseModel::createNewPost_($this, $post);
    }
    
    function updatePost($post) {
    	\App\Models\PostsBaseModel::updatePost_($this, $post);
    }

    function author() {
    	return $this->belongsTo(\App\User::class, AUTHOR, EMAIL);
    }
    
    function comments() {
    	return $this->hasMany(\App\Models\PostComments::class, POST_ID, TABLE_ID);
    }
    
}

