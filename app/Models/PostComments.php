<?php

namespace App\Models;

class PostComments extends PostsBaseModel { 
	
    public $table = POST_COMMENTS_TABLE;
    
    protected $fillable = array(TITLE, AUTHOR, CONTENT, EMAIL, POST_ID, TAG);
    
    function createNewPostComment($post) {
    	\App\Models\PostsBaseModel::createNewPostComment_($this, $post);
    }
    
    function updatePostComment($post) {
    	\App\Models\PostsBaseModel::updatePostComment_($this, $post);
    }

    function post() {
    	return $this->belongsTo(\App\Models\Post::class, POST_ID, TABLE_ID);
    }
    
}

