<?php

// use Faker\Provider\Lorem;
// require 'C:/wamp/www/blog/vendor/unisharp/laravel-filemanager/src/routes.php';

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::group(['middleware' => ['web']], function(){
	
	Route::get('/', ['as' => 'home', 'uses' => 'Blog\Blog@index']);
	Route::any('/view-post/{postTitle}', 	['as' => 'view_post', 'uses' => 'Blog\Blog@viewPost']);
	Route::any('/view-ireport/{postTitle}', ['as' => 'view_ireport', 'uses' => 'Blog\Blog@viewIReportPost']);
	Route::get('/view-profile/{email}', ['as' => 'view_profile', 'uses' => 'Blog\Blog@viewAuthorProfile']);
	Route::get('/our-product-store/', 	['as' => 'view_our_products', 'uses' => 'Blog\Blog@viewOurProducts']);
	Route::get('/view-product/{title}', ['as' => 'view_product', 'uses' => 'Blog\Blog@viewProduct']);
	Route::get('/view-section/{category}', ['as' => 'view_category', 'uses' => 'Blog\Blog@viewCategory']);
	Route::get('/view-section/{category}/{sub_category}', ['as' => 'view_sub_category', 'uses' => 'Blog\Blog@viewSubCategory']);
	Route::get('/view-store-section/{category}', ['as' => 'view_store_section', 'uses' => 'Blog\Blog@viewStoreSection']);
	
	
	Route::get('/user/dashboard', ['as' => 'user_home', 'uses' => 'User\User@index']);
	Route::get('/user/dashboard/view-posts', 	['as' => 'view_all_posts', 'uses' => 'User\Post@viewPosts']);
	Route::get('/user/dashboard/view-my-posts', ['as' => 'view_my_posts', 'uses' => 'User\Post@viewMyPosts']);
	Route::any('/user/dashboard/create-post', 	['as' => 'create_post', 'uses' => 'User\Post@createPost']);
	Route::any('/user/dashboard/edit-post/{tableID}', 	['as' => 'edit_post', 'uses' => 'User\Post@editPost']);
	Route::get('/user/dashboard/preview-post/{title}', 	['as' => 'preview_post', 'uses' => 'User\Post@previewPost']);
	Route::get('/user/dashboard/delete-post/{tableID}', ['as' => 'delete_post', 'uses' => 'User\Post@deletePost']);
	
	// For IReport
	Route::get('/user/dashboard/ireport/view-posts', 	['as' => 'ir_view_all_posts', 'uses' => 'User\IReport@viewPosts']);
	Route::get('/user/dashboard/ireport/view-my-posts', ['as' => 'ir_view_my_posts', 'uses' => 'User\IReport@viewMyPosts']);
	Route::any('/user/dashboard/ireport/create-post', 	['as' => 'ir_create_post', 'uses' => 'User\IReport@createPost']);
	Route::any('/user/dashboard/ireport/edit-post/{tableID}', 	['as' => 'ir_edit_post', 'uses' => 'User\IReport@editPost']);
	Route::get('/user/dashboard/ireport/preview-post/{title}', 	['as' => 'ir_preview_post', 'uses' => 'User\IReport@previewPost']);
	Route::get('/user/dashboard/ireport/delete-post/{tableID}', ['as' => 'ir_delete_post', 'uses' => 'User\IReport@deletePost']);
	
	// For Store
	Route::get('/user/dashboard/store/view-product', 	['as' => 'store_view_all', 'uses' => 'User\Store@viewAll']);
	Route::any('/user/dashboard/store/create-product', 	['as' => 'store_new_product', 'uses' => 'User\Store@addNewProduct']);
	Route::any('/user/dashboard/store/edit-product/{tableID}', 	['as' => 'store_edit_product', 'uses' => 'User\Store@editProduct']);
	Route::get('/user/dashboard/store/preview-product/{title}', ['as' => 'store_preview_product', 'uses' => 'User\Store@previewProduct']);
	Route::get('/user/dashboard/store/delete-product/{tableID}',['as' => 'store_delete_product', 'uses' => 'User\Store@deleteProduct']);



Route::get('/viewCbtagent','Blog\Blog@viewCbtagent');
Route::get('/utmeSyllabus','Blog\Blog@utmeSyllabus');
Route::get('/accounting','Blog\Blog@accounting');
Route::get('/contact','Blog\Blog@getContact');
Route::post('/contact','Blog\Blog@postContact');
Route::get('/forum','Blog\Blog@Forum');


//Route::post('/contact',array('as'=>'insertContact','uses'=>'Blog\Blog@contact'));





// });

// Route::get('post', ['as' => 'post', 'uses' => 'PostviewsController@index']);


Route::auth();

// Route::get('/home', 'HomeController@index');

Route::get('/rough', function () {
// 	dDie(url('/storage/app/public/images/store/'));
 	require storage_path() . '/../tests/faker.php';
 	seedAllTables();
});

Route::get('/update-repo', function () {
	require_once storage_path() . '/../git_update.php';
});





