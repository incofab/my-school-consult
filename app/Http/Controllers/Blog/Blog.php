<?php

namespace App\Http\Controllers\Blog;
use App\Http\Requests;

use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use App\Post;
use Mail;
class Blog extends Controller{


    public  function index(){
    	
    	$postObj = new \App\Models\Post();
    	
    	$latestNews = $postObj->where(CATEGORY, '=', CATEGORY_GENERAL)->orderBy(TABLE_ID, 'DESC')->take(5)->get();
    	$internshipNews = $postObj->where(CATEGORY, '=', CATEGORY_INTERNSHIP)->orderBy(TABLE_ID, 'DESC')->take(5)->get();
    	$scholarshipNews = $postObj->where(CATEGORY, '=', CATEGORY_SCHOLARSHIP)->orderBy(TABLE_ID, 'DESC')->take(5)->get();
    	$nyscNews = $postObj->where(CATEGORY, '=', CATEGORY_NYSC)->orderBy(TABLE_ID, 'DESC')->take(5)->get();

    	$iReportNews = \App\Models\IReport::orderBy(TABLE_ID, 'DESC')->take(5)->get();

    	$products = \App\Models\Store::orderBy(TABLE_ID, 'DESC')->take(4)->get();
    	
    	return view('blog.index', [
    			'latestNews' => $latestNews,
    			'internshipNews' => $internshipNews,
    			'scholarshipNews' => $scholarshipNews,
    			'nyscNews' => $nyscNews,
    			'iReportNews' => $iReportNews,    			
    			'products' => $products,    			
    	]);

    }
    
    function viewPost($title) {
    	
    	if(Request::get('post_comment')){
    		(new \App\Models\PostComments())->createNewPostComment(Request::all());
    		redirect()->to(URL::current())->send();
    	}
    	
    	$blogPost = \App\Models\Post::where(TITLE_URL, '=', $title)->first();
    	
    	if(!$blogPost){
    		redirect()->route('home')->with(ERROR_MSG, 'Post not found')->send();	
    	}
    	
    	$comments = $blogPost->comments()->orderBy(TABLE_ID, 'DESC')->take(20)->get();

    	return view('blog.view_post', [
    			'blogPost' => $blogPost,
    			'comments' => $comments,
    			'post' => Session::get('post')
    	]);
    	
    }
    
    function viewIReportPost($title) {
    	
    	if(Request::get('post_comment')){
    		(new \App\Models\IReport())->createNewPostComment(Request::all());
    		redirect()->to(URL::current())->send();
    	}
    	
    	$blogPost = \App\Models\IReport::where(TITLE_URL, '=', $title)->first();
    	
    	if(!$blogPost){
    		redirect()->route('home')->with(ERROR_MSG, 'Post not found')->send();	
    	}
    	
    	$comments = $blogPost->comments()->orderBy(TABLE_ID, 'DESC')->take(20)->get();
    	
    	return view('blog.view_post', [
    			'blogPost' => $blogPost,
    			'comments' => $comments,
    			'post' => Session::get('post')
    	]);
    	
    }
    
    function viewOurProducts() {
    	
    	$products = \App\Models\Store::orderBy(TABLE_ID, 'DESC')->take(24)->get();
    	$products = $products->toArray();
    	
    	return view('blog.view_stores', [
    			'products' => $products,
    	]);
    	
    }

    function viewProduct($title) {
    	    	 
    	$product = \App\Models\Store::where(TITLE_URL, '=', $title)->first();
    	 
    	if(!$product){
    		redirect()->route('view_our_products')->with(ERROR_MSG, 'Product not found')->send();
    	}
    	dDie($product);
    	return view('blog.view_product', [
    			'product' => $product,
    	]);
    	 
    }
    
    function viewAuthorProfile($email) {
    	
    	$profile = \App\User::where(EMAIL, '=', $email)->first();
    	
    	return view("blog.profileview", [
    			'profile' => $profile
    	]);
    }
    
    function viewCategory($category) {
    	
	    if($category == CATEGORY_GENERAL){
	    	$folder = 'news';
	    	$ret = $this->getGeneralNewsPosts();
	    }elseif($category == CATEGORY_INTERNSHIP){
	    	$folder = 'internship';
	    	$ret = $this->getInternshipPosts();
	    }elseif($category == CATEGORY_SCHOLARSHIP){
	    	$folder = 'scholarship';
	    	$ret = $this->getScholarshipsPosts();
	    }else return null;
	    
	    $ret['folder'] = $folder;
	    
    	return view("blog.$folder.view", $ret);
    }
    
    private function getGeneralNewsPosts() {
    	
    	$admissionNews = $this->retrievePostSubCat
    			(CATEGORY_GENERAL, SUB_CAT_ADMISSION_NEWS);
    	$jambNews = $this->retrievePostSubCat
    			(CATEGORY_GENERAL, SUB_CAT_JAMB_NEWS);
    	$latestNews = $this->retrievePostSubCat
    			(CATEGORY_GENERAL, SUB_CAT_LATEST_NEWS);
    	$nyscNews = $this->retrievePostSubCat
    			(CATEGORY_GENERAL, SUB_CAT_LATEST_NEWS);
    	$postUTMENews = $this->retrievePostSubCat
    			(CATEGORY_GENERAL, SUB_CAT_POST_UTME_NEWS);
    	$IReport = $this->retrievePostSubCat
    			(CATEGORY_GENERAL, SUB_CAT_UNIVERSITY_IREPORT);
    	
    	return [
    			'jambNews' => $jambNews,
    			'latestNews' => $latestNews,
    			'admissionNews' => $admissionNews,
    			'nyscNews' => $nyscNews,
    			'IReport' => $IReport,
    			'postUTMENews' => $postUTMENews,
    	];				
    }
    
    private function getInternshipPosts() {
    	
    	$industrialTraining = $this->retrievePostSubCat
    			(CATEGORY_INTERNSHIP, SUB_CAT_INDUSTRIAL_TRAINING);
    	$graduateInternship = $this->retrievePostSubCat
    			(CATEGORY_INTERNSHIP, SUB_CAT_GRADUATE_INTERNSHIP);
    	$underGraduateInternship= $this->retrievePostSubCat
    			(CATEGORY_INTERNSHIP, SUB_CAT_UNDERGRADUATE_INTERNSHIP);
    	
    	return [
    			'industrialTraining' => $industrialTraining,
    			'graduateInternship' => $graduateInternship,
    			'underGraduateInternship' => $underGraduateInternship,
    	];				
    }
    
    private function getScholarshipsPosts() {
    	
    	$internationalScholarship = $this->retrievePostSubCat
    			(CATEGORY_SCHOLARSHIP, SUB_CAT_INTERNATIONAL_SCHOLARSHIP);
    	$postGraduateScholarship = $this->retrievePostSubCat
    			(CATEGORY_SCHOLARSHIP, SUB_CAT_GRADUATE_SCHOLARSHIP);
    	$graduateScholarship = $this->retrievePostSubCat
    			(CATEGORY_SCHOLARSHIP, SUB_CAT_POST_SCHOLARSHIP);
    	$underGraduateScholarship = $this->retrievePostSubCat
    			(CATEGORY_SCHOLARSHIP, SUB_CAT_UNDERGRADUATE_SCHOLARSHIP);
    	
    	return [
    			'internationalScholarship' => $internationalScholarship,
    			'postGraduateScholarship' => $postGraduateScholarship,
    			'graduateScholarship' => $graduateScholarship,
    			'underGraduateScholarship' => $underGraduateScholarship,
    	];				
    }
    
    private function retrievePostSubCat($category, $subCat, $count = 15) {
    	return \App\Models\Post::where(CATEGORY, '=', $category)
	    	->where(SUB_CATEGORY, '=', $subCat)
	    	->orderBy(TABLE_ID, 'DESC')->take(30)->get();
    }
    
    function viewSubCategory($category, $subCategory) {
    	
    	$posts = \App\Models\Post::where(CATEGORY, '=', $category)
    			->where(SUB_CATEGORY, '=', $subCategory)
    			->orderBy(TABLE_ID, 'DESC')->take(30)->get();

		if($category == CATEGORY_GENERAL){
			$folder = 'news';
		}elseif($category == CATEGORY_INTERNSHIP){
			$folder = 'internship';
		}elseif($category == CATEGORY_SCHOLARSHIP){
			$folder = 'scholarship';
		}else return null;
    	
    	return view("blog.$folder.view_sub_category", [
    	   		'posts' => $posts,
    			'heading' => htmlentities($subCategory),
    			'folder' => $folder,
    	]);
    	
    }
    
    function viewStoreSection($category) {
    	
    	$products = \App\Models\Store::where(CATEGORY, '=', $category)
    			->orderBy(TABLE_ID, 'DESC')->take(30)->get();
    	
    	$products = $products->toArray();
    			 
    	return view('blog.view_stores', [
    			'products' => $products,
    	]);
    	
    }

    function viewcbtAgent(){
 return view('blog.jambsyllabus.viewCbtagent');
    
}
 function utmeSyllabus(){
 return view('blog.jambsyllabus.utmeSyllabus');
    
}
function accounting(){
  return view('blog.jambsyllabus.accounting');
    
} 


    public function getContact()
    {
    return view('blog.contact');
}
 public function Forum()
    {
    return view('blog.gist.forum');
}

public function postContact(Request $request)
    {
      $this->validate($request, [
        'email' => 'required|email',
        'subject' =>'min:3',
        'message'=>'min:10']);
      $data = array(
        'email'=>$request->email,
        'subject'=>$request->subject, 
        'bodyMessage'=>$request->message
        );

      Mail::send('blog.emails.contact',$data, function($message) use ($data){
        $message->from($data['email']);
        $message->to('uc7mille7@gmail.com');
        $message->subject($data['subject']);



      });
    }


}

