<?php

namespace App\Http\Controllers\User;


use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
class Post extends PostsBaseController {
	
	protected $heading = 'New post';
	protected $editHeading = 'Edit post';
	
	// View my post route name
	protected $viewMyPost = 'view_my_posts';
	protected $deletePost = 'delete_post';
	protected $createNewPost = 'create_post';
	protected $editPost = 'edit_post';
	protected $previewPost = 'preview_post';
	protected $folder = 'post';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct(new \App\Models\Post());
    }
   
    
}
