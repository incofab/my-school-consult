<?php

namespace App\Http\Controllers\User;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class IReport extends PostsBaseController {
	
	protected $heading = 'New Scholarship post';
	protected $editHeading = 'Edit Scholarship post';
	
	// View my post route name
	protected $viewMyPost = 'ir_view_my_posts';
	protected $deletePost = 'ir_delete_post';
	protected $createNewPost = 'ir_create_post';
	protected $editPost = 'ir_edit_post';
	protected $previewPost = 'ir_preview_post';
	protected $folder = 'ireport';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct(new \App\Models\IReport());
    }
    
    
}
