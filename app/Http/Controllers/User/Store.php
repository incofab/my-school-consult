<?php
namespace App\Http\Controllers\User;


use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class Store extends User {
	
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct(){
        parent::__construct();
    }
    
    public function viewAll(){
    	
        return view("user.store.view_all", 
        		[
        			'data' => Auth::user(),
        			'posts' => \App\Models\Store::all(),
        		]);
    }
    
    public function addNewProduct(){
    	
    	if(Request::get('add_new')){
    		(new \App\Models\Store)->addNewProduct(Request::all());
    	}
    	
    	if(Session::get(SUCCESS_MSG)){
    		Session::reFlash();
    		redirect()->route('store_view_all')->send();
    	}
    	
        return view("user.store.add_new", 
        		[
        			'data' => Auth::user(),
        			'post' => Session::get('post'),
        		]);
        
    }
    
    public function editProduct($tableID){

    	if(Request::get('edit_product')){
    		(new \App\Models\Store)->updateProduct(Request::all());
    	}
    	 
    	if(Session::get(SUCCESS_MSG)){
    		Session::reFlash();
    		redirect()->route('store_view_all')->send();
    	}
    	
    	$post = Session::has('post') ? Session::get('post') 
    			: \App\Models\Store::where(TABLE_ID, '=', $tableID)
    				->first();
    	
        return view("user.store.add_new", [
        		'edit' => true,
        		'post' => $post,
        		'data' => Auth::user(),
        ]);
    }
    
    public function previewProduct($title){
    	
    	$post = \App\Models\Store::where(TITLE_URL, '=', $title);
    	
        return view("user.store.preview", ['post' => $post, 'data' => Auth::user(),]);
    }
    
    public function deleteProduct($tableID){

    	$success = \App\Models\Store::where(TABLE_ID, '=', $tableID)->delete();
    	
    	if ($success){
    		Session::flash(SUCCESS_MSG, 'Post deleted successfully');
    	}else {
    		\Session::flash(ERROR_MSG, 'Delete failed');
    	}
    	
    	redirect()->route('store_view_all')->send();
    	
    }
    
    
}