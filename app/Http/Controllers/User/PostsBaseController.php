<?php
namespace App\Http\Controllers\User;


use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class PostsBaseController extends User {
	
	private $postObj;
// 	protected $heading = null;//'New post';
// 	protected $editHeading = null;//'Edit post';

// 	protected $viewMyPost = null;//'view_my_posts';
// 	protected $deletePost = null;//'view_my_posts';
// 	protected $createNewPost = null;//'view_my_posts';
// 	protected $editPost = null;//'view_my_posts';
// 	protected $previewPost = null;//'view_my_posts';
// 	protected $folder = null;
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct(\App\Models\BaseModel $obj){
    	$this->postObj = $obj;
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('home');
    }
    
    public function viewMyPosts(){
    	
        return view("user.{$this->folder}.view_posts", 
        		[
        			'data' => Auth::user(),
        			'posts' => $this->postObj->where(AUTHOR, '=', Auth::user()[EMAIL])->orderBy(TABLE_ID, 'DESC')->get(),
        			'edit_post' => $this->editPost,
        			'delete_post' => $this->deletePost,
        			'preview_post' => $this->previewPost,
        			'create_post' => $this->createNewPost,
        		]);
    }
    
    public function viewPosts(){
    	
        return view("user.{$this->folder}.view_posts", 
        		[
        			'data' => Auth::user(),
        			'posts' => $this->postObj->orderBy(TABLE_ID, 'DESC')->all(),
        			'edit_post' => $this->editPost,
        			'delete_post' => $this->deletePost,
        			'preview_post' => $this->previewPost,
        			'create_post' => $this->createNewPost,
        		]);
    }
    
    public function createPost(){
    	
    	if(Request::get('create_new')){
    		$this->postObj->createNewPost(Request::all());
    	}
    	
    	if(Session::get(SUCCESS_MSG)){
    		Session::reFlash();
    		redirect()->route($this->viewMyPost)->send();
    	}
    	
        return view("user.{$this->folder}.create_post", 
        		[
        			'heading' => $this->heading, 
        			'data' => Auth::user(),
        			'post' => Session::get('post'),
        		]);
        
    }
    
    public function editPost($tableID){

    	if(Request::get('edit_post')){
    		$this->postObj->updatePost(Request::all());
    	}
    	 
    	if(Session::get(SUCCESS_MSG)){
    		Session::reFlash();
    		redirect()->route($this->viewMyPost)->send();
    	}
    	
    	$post = Session::has('post') ? Session::get('post') 
    			: $this->postObj->where(TABLE_ID, '=', $tableID)
    				->where(AUTHOR, '=', Auth::user()[EMAIL])->first();
    	
        return view("user.{$this->folder}.create_post", [
        		'heading' => $this->editHeading, 
        		'edit' => true,
        		'post' => $post,
        		'data' => Auth::user(),
        ]);
    }
    
    public function previewPost($title){
    	
    	$post = $this->postObj->where(TITLE_URL, '=', $title)->get();
    	
        return view("user.{$this->folder}.preview_post", ['post' => $post, 'data' => Auth::user(),]);
    }
    
    public function deletePost($tableID){

    	$success = $this->postObj->where(TABLE_ID, '=', $tableID)
    		->where(AUTHOR, '=', Auth::user()[EMAIL])->delete();
    	
    	if ($success){
    		Session::flash(SUCCESS_MSG, 'Post deleted successfully');
    	}else {
    		\Session::flash(ERROR_MSG, 'Delete failed');
    	}
    	
    	redirect()->route($this->viewMyPost)->send();
    	
    }
    
    
}