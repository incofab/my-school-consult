<?php

use Faker\Provider\Lorem;
use Faker\Factory;
// require_once __DIR__ . '/Faker/src/autoload.php';
// require_once 'C:/Users/Incofab/Desktop/libraries/php library/Faker-master/Faker/src/autoload.php';

// $faker = Faker\Factory::create();

function seedAllTables(){
	$faker = Factory::create();
	seed_main_post_table($faker);
	seed_ireport_table($faker);
	seed_store_table($faker);
}

function seed_main_post_table($faker) {
	seed_post_table(new \App\Models\Post(), $faker);
}
function seed_ireport_table($faker) {
	seed_post_table(new \App\Models\IReport(), $faker);
}
function seed_post_table($obj, $faker) {
	
	for ($i = 0; $i < 100; $i++){
		
		$cat = Lorem::randomElement(getAllCategories());
		$subcatArr = getAllSubCategories()[$cat];
		
		$title = Lorem::sentence(8);
		$obj->create([
				AUTHOR => Lorem::randomElement(['email@yahoo.com']),//, 'inco@yahoo.com', 'incofab@yahoo.com']), 
				TITLE => $title,
				TITLE_URL => $obj->createURLFromTitle($title),
				CONTENT => Lorem::paragraphs(6, true),
				SHORT_DESC => Lorem::sentence(15),
				TAG => Lorem::words(7, true),
				CATEGORY => $cat, 
				SUB_CATEGORY => Lorem::randomElement($subcatArr), 
		]);
	}
}

function seed_store_table($faker) {

	for ($i = 0; $i < 50; $i++){
		
		$title = Lorem::sentence(8);
		
		$obj = new \App\Models\Store();
		$obj->create([
				TITLE => Lorem::sentence(8),
				TITLE_URL => $obj->createURLFromTitle($title),
				DESCRIPTION => Lorem::paragraphs(6, true),
				TAG => Lorem::words(7, true),
				IMAGES => 'jamb-cbt-software-myschool.png',
				CATEGORY => Lorem::randomElement(getAllStoreCategories()), 
				PRICE => Lorem::randomElement([2800, 4500, 2000, 300, 1000, 1500, 2500, 500, 4000]), 
		]);
		
	}
	
}




echo '<br />done <br />';